/* Written by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>f
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module xreader is aliced;

import core.time;

import std.concurrency;
import std.net.curl;
import std.regex;

import arsd.simpledisplay;
import arsd.image;

import iv.nanovega;
import iv.nanovega.textlayouter;
import iv.nanovega.blendish;
import iv.nanovega.perf;

import iv.strex;

import iv.cmdcongl;

import iv.unarray;

import iv.vfs;
import iv.vfs.io;

import xmodel;
import xiniz;

import booktext;

import xreadercfg;
import xreaderui;
import xreaderifs;
import xreaderfmt;

import eworld;
import ewbillboard;

//version = debug_draw;


// ////////////////////////////////////////////////////////////////////////// //
__gshared int oglConScale = 1;


// ////////////////////////////////////////////////////////////////////////// //
__gshared string bookFileName;

__gshared int formatWorks = -1;
__gshared NVGContext vg = null;
__gshared PerfGraph fps;
__gshared bool fpsVisible = false;
__gshared BookText bookText;
__gshared string newBookFileName;
__gshared uint[] posstack;
__gshared SimpleWindow sdwindow;
__gshared LayTextC laytext;
__gshared BookInfo[] recentFiles;
__gshared BookMetadata bookmeta;
__gshared int currentSection = -666;
__gshared bool mouseStartMarking = false;

__gshared int textHeight;

__gshared int toMove = 0; // for smooth scroller
__gshared int topY = 0;
__gshared int arrowDir = 0;
__gshared int lastArrowDir = 0;
__gshared float arrowSpeed = 0;

__gshared int newYLine = -1; // index
__gshared float newYAlpha;
__gshared bool newYFade = false;
__gshared MonoTime nextFadeTime;

__gshared NVGImage curImg, curImgWhite;
__gshared int mouseX = -666, mouseY = -666;
__gshared bool mouseHigh = false;
__gshared bool mouseHidden = false;
__gshared MonoTime lastMMove;
__gshared float mouseAlpha = 1.0f;
__gshared int mouseFadingAway = false;

__gshared bool needRedrawFlag = true;

__gshared bool firstFormat = true;

__gshared bool inGalaxyMap = false;
__gshared PopupMenu currPopup;
__gshared void delegate (int item) onPopupSelect;
__gshared bool delegate (KeyEvent event) onPopupKeyEvent; // return `true` to close menu
__gshared int shipModelIndex = 0;
__gshared bool popupNoShipKill = false;

__gshared bool doSaveCheck = false;
__gshared MonoTime nextSaveTime;


// ////////////////////////////////////////////////////////////////////////// //
struct HrefInfo {
  int widx;
  int x0, y0;
  int width, height;
}

// all visible hrefs on a page
__gshared HrefInfo[] hreflist;
__gshared int hrefcurr = -1;


// ////////////////////////////////////////////////////////////////////////// //
struct Pulse {
public:
  float PulseScale = 8; // ratio of "tail" to "acceleration"

private:
  float PulseNormalize = 1;

private:
  // viscous fluid with a pulse for part and decay for the rest
  float pulseInternal (float x) nothrow @safe @nogc {
    import std.math : exp;
    float val;

    // test
    x = x*PulseScale;
    if (x < 1) {
      val = x-(1-exp(-x));
    } else {
      // the previous animation ended here:
      float start = exp(-1.0f);
      // simple viscous drag
      x -= 1;
      float expx = 1-exp(-x);
      val = start+(expx*(1.0-start));
    }

    return val*PulseNormalize;
  }


  void ComputePulseScale () nothrow @safe @nogc {
    PulseNormalize = 1.0f/pulseInternal(1);
  }

public:
  this (float ascale) nothrow @safe @nogc { PulseScale = ascale; }

  void setScale (float ascale) nothrow @safe @nogc { PulseScale = ascale; PulseNormalize = 1; }

  // viscous fluid with a pulse for part and decay for the rest
  float pulse (float x) nothrow @safe @nogc {
    if (x >= 1) return 1;
    if (x <= 0) return 0;

    if (PulseNormalize == 1) ComputePulseScale();

    return pulseInternal(x);
  }
}


__gshared Pulse pulse;


// ////////////////////////////////////////////////////////////////////////// //
void refresh () { needRedrawFlag = true; }
void refreshed () { needRedrawFlag = false; }

bool needRedraw () {
  return (needRedrawFlag || (currPopup !is null && currPopup.needRedraw));
}


@property bool inMenu () { return (currPopup !is null); }
void closeMenu () { if (currPopup !is null) currPopup.destroy; currPopup = null; onPopupSelect = null; onPopupKeyEvent = null; }


void setShip (int idx) {
  if (eliteShipFiles.length) {
    import core.memory : GC;
    if (idx >= eliteShipFiles.length) idx = cast(int)eliteShipFiles.length-1;
    if (idx < 0) idx = 0;
    if (shipModelIndex == idx && shipModel !is null) return;
    // remove old ship
    shipModelIndex = idx;
    if (shipModel !is null) {
      shipModel.glUnload();
      shipModel.freeData();
      shipModel.destroy;
      shipModel = null;
    }
    GC.collect();
    // load new ship
    try {
      shipModel = new EliteModel(eliteShipFiles[shipModelIndex]);
      shipModel.glUpload();
      shipModel.freeImages();
    } catch (Exception e) {}
    //shipModel = eliteShips[shipModelIndex];
    GC.collect();
  }
}

void ensureShipModel () {
  if (eliteShipFiles.length && shipModel is null) {
    import std.random : uniform;
    setShip(uniform!"[)"(0, eliteShipFiles.length));
  }
}

void freeShipModel () {
  if (!showShip && !inMenu && shipModel !is null) {
    import core.memory : GC;
    shipModel.glUnload();
    shipModel.freeData();
    shipModel.destroy;
    shipModel = null;
    GC.collect();
  }
}


void loadState () {
  try {
    int widx = getCurrentFileWordIndex();
    /*
    xiniParse(VFile(stateFileName),
      "wordindex", &widx,
    );
    */
    if (widx >= 0) goTo(widx);
  } catch (Exception) {}
}

void doSaveState (bool forced=false) {
  if (!forced) {
    if (formatWorks != 0) return;
    if (!doSaveCheck) return;
    auto ct = MonoTime.currTime;
    if (ct < nextSaveTime) return;
  } else {
    if (laytext is null || laytext.lineCount == 0 || formatWorks != 0) return;
  }
  try {
    //auto fo = VFile(stateFileName, "w");
    if (laytext !is null && laytext.lineCount) {
      auto lnum = laytext.findLineAtY(topY);
      if (lnum >= 0) {
        //fo.writeln("wordindex=", laytext.line(lnum).wstart);
        updateCurrentFileWordIndex(laytext.line(lnum).wstart);
      }
    }
  } catch (Exception) {}
  doSaveCheck = false;
}

void stateChanged () {
  if (!doSaveCheck) {
    doSaveCheck = true;
    nextSaveTime = MonoTime.currTime+10.seconds;
  }
}


void hardScrollBy (int delta) {
  if (delta == 0 || laytext is null || laytext.lineCount == 0) return;
  int oldY = topY;
  topY += delta;
  if (topY >= laytext.textHeight-textHeight) topY = cast(int)laytext.textHeight-textHeight;
  if (topY < 0) topY = 0;
  if (topY != oldY) {
    stateChanged();
    refresh();
  }
}

void scrollBy (int delta) {
  if (delta == 0 || laytext is null || laytext.lineCount == 0) return;
  toMove += delta;
  newYFade = true;
  newYAlpha = 1;
  if (delta < 0) {
    // scrolling up, mark top line
    newYLine = laytext.findLineAtY(topY);
  } else {
    // scrolling down, mark bottom line
    newYLine = laytext.findLineAtY(topY+textHeight-2);
  }
  version(none) {
    conwriteln("scrollBy: delta=", delta, "; newYLine=", newYLine, "; topLine=", laytext.findLineAtY(topY));
  }
  if (newYLine < 0) newYFade = false;
}


void goHome () {
  if (laytext is null) return;
  if (topY != 0) {
    topY = 0;
    stateChanged();
    refresh();
  }
}


void goEnd () {
  if (laytext is null || laytext.lineCount < 2) return;
  auto newY = laytext.line(laytext.lineCount-1).y;
  if (newY >= laytext.textHeight-textHeight) newY = cast(int)laytext.textHeight-textHeight;
  if (newY < 0) newY = 0;
  if (newY != topY) {
    topY = newY;
    stateChanged();
    refresh();
  }
}


void goTo (uint widx) {
  if (laytext is null) return;
  auto lidx = laytext.findLineWithWord(widx);
  if (lidx != -1) {
    assert(lidx < laytext.lineCount);
    toMove = 0;
    if (topY != laytext.line(lidx).y) {
      topY = laytext.line(lidx).y;
      stateChanged();
      refresh();
    }
    version(none) {
      conwriteln("goto: widx=", widx, "; lidx=", lidx, "; fwn=", laytext.line(lidx).wstart, "; topY=", topY);
      auto lnum = laytext.findLineAtY(topY);
      conwriteln("  newlnum=", lnum, "; lidx.y=", laytext.line(lidx).y, "; lnum.y=", laytext.line(lnum).y);
    }
  }
}


void pushPosition () {
  if (laytext is null || laytext.lineCount == 0) return;
  auto lidx = laytext.findLineAtY(topY);
  if (lidx >= 0) posstack ~= laytext.line(lidx).wstart;
}

void popPosition () {
  if (posstack.length == 0) return;
  auto widx = posstack[$-1];
  posstack.length -= 1;
  posstack.assumeSafeAppend;
  goTo(widx);
}


void gotoSection (int sn) {
  if (laytext is null || laytext.lineCount == 0 || bookmeta is null) return;
  if (sn < 0 || sn >= bookmeta.sections.length) return;
  auto lidx = laytext.findLineWithWord(bookmeta.sections[sn].wordidx);
  if (lidx >= 0) {
    auto newY = laytext.line(lidx).y;
    if (newY != topY) {
      topY = newY;
      stateChanged();
      refresh();
    }
  }
}


void relayout (bool forced=false) {
  if (laytext !is null) {
    uint widx;
    auto lidx = laytext.findLineAtY(topY);
    if (lidx >= 0) widx = laytext.line(lidx).wstart;
    int maxWidth = GWidth-4-2-BND_SCROLLBAR_WIDTH-2;
    //if (maxWidth < MinWinWidth) maxWidth = MinWinWidth;
    {
      import core.time;
      auto stt = MonoTime.currTime;
      laytext.relayout(maxWidth, forced);
      auto ett = MonoTime.currTime-stt;
      conwriteln("relayouted in ", ett.total!"msecs", " milliseconds; lines:", laytext.lineCount, "; words:", laytext.nextWordIndex);
    }
    goTo(widx);
    refresh();
  }
}


void drawShipName () {
  if (shipModel is null || shipModel.name.length == 0) return;
  vg.fontFaceId(uiFont);
  vg.textAlign(NVGTextAlign.H.Left, NVGTextAlign.V.Baseline);
  vg.fontSize(fsizeUI);
  auto w = vg.bndLabelWidth(-1, shipModel.name)+8;
  float h = BND_WIDGET_HEIGHT+8;
  float mx = (GWidth-w)/2.0;
  float my = (GHeight-h)-8;
  vg.bndMenuBackground(mx, my, w, h, BND_CORNER_NONE);
  vg.bndMenuItem(mx+4, my+4, w-8, BND_WIDGET_HEIGHT, BND_DEFAULT, -1, shipModel.name);
  if (shipModel.dispName && shipModel.dispName != shipModel.name) {
    my -= BND_WIDGET_HEIGHT+16;
    w = vg.bndLabelWidth(-1, shipModel.dispName)+8;
    mx = (GWidth-w)/2.0;
    vg.bndMenuBackground(mx, my, w, h, BND_CORNER_NONE);
    vg.bndMenuItem(mx+4, my+4, w-8, BND_WIDGET_HEIGHT, BND_DEFAULT, -1, shipModel.dispName);
  }
}


void setWindowTitle (bool force=false) {
  if (bookmeta is null || laytext is null) {
    currentSection = -666;
    return;
  }
  int scidx = -1;
  if (bookmeta.sections.length > 1) {
    auto lidx = laytext.findLineAtY(topY);
    if (lidx >= 0) {
      foreach (auto idx, ref cc; bookmeta.sections) {
        if (idx == 0) continue;
        auto sline = laytext.findLineWithWord(cc.wordidx);
        if (sline >= 0 && lidx >= sline) scidx = cast(int)idx;
      }
    }
  }
  //Anc sc = null;
  //if (scidx > 0 && scidx < bookmeta.sections.length) sc = bookmeta.sections[scidx];
  if (force || scidx != currentSection) {
    currentSection = scidx;
    string tt;
    if (scidx > 0 && scidx < bookmeta.sections.length) {
      import std.conv : to;
      tt = bookmeta.sections[scidx].name.to!string~" \xe2\x80\x94 ";
    }
    sdwindow.title = tt~bookText.title~" \xe2\x80\x94 "~bookText.authorFirst~" "~bookText.authorLast;
  }
}


void createSectionMenu () {
  closeMenu();
  //conwriteln("lc=", laytext.lineCount, "; bm: ", (bookmeta !is null));
  if (laytext is null || laytext.lineCount == 0 || bookmeta is null) { freeShipModel(); return; }
  currPopup = new PopupMenu(vg, "Sections"d, () {
    dstring[] items;
    foreach (const ref sc; bookmeta.sections) items ~= sc.name;
    return items;
  });
  currPopup.allowFiltering = true;
  //conwriteln(currPopup.items.length);
  // find current section
  currPopup.itemIndex = 0;
  auto lidx = laytext.findLineAtY(topY);
  if (lidx >= 0 && bookmeta.sections.length > 0) {
    foreach (immutable sidx, const ref sc; bookmeta.sections) {
      auto sline = laytext.findLineWithWord(sc.wordidx);
      if (sline >= 0 && lidx >= sline) currPopup.itemIndex = cast(int)sidx;
    }
  }
  onPopupSelect = (int item) { gotoSection(item); };
}


void createQuitMenu (bool wantYes) {
  closeMenu();
  currPopup = new PopupMenu(vg, "Quit?"d, () {
    return ["Yes"d, "No"d];
  });
  currPopup.itemIndex = (wantYes ? 0 : 1);
  onPopupSelect = (int item) { if (item == 0) concmd("quit"); };
}


void createRecentMenu () {
  closeMenu();
  if (recentFiles.length == 0) recentFiles = loadDetailedHistory();
  if (recentFiles.length == 0) { freeShipModel(); return; }
  currPopup = new PopupMenu(vg, "Recent files"d, () {
    import std.conv : to;
    dstring[] res;
    foreach (const ref BookInfo bi; recentFiles) {
      string s = bi.title;
      if (bi.seqname.length) {
        s ~= " (";
        if (bi.seqnum) { s ~= to!string(bi.seqnum); s ~= ": "; }
        //conwriteln(bi.seqname);
        s ~= bi.seqname;
        s ~= ")";
      }
      if (bi.author.length) { s ~= " \xe2\x80\x94 "; s ~= bi.author; }
      res ~= s.to!dstring;
    }
    return res;
  });
  currPopup.allowFiltering = true;
  currPopup.itemIndex = cast(int)recentFiles.length-1;
  onPopupSelect = (int item) {
    newBookFileName = recentFiles[item].diskfile;
    popupNoShipKill = true;
  };
  onPopupKeyEvent = (KeyEvent event) {
    if (event == "Delete" && currPopup.isCurValid) {
      auto idx = currPopup.itemIndex;
      if (idx >= 0 && idx < recentFiles.length) {
        removeFileFromHistory(recentFiles[idx].diskfile);
        currPopup.removeItem(idx);
        foreach (immutable c; idx+1..recentFiles.length) recentFiles[c-1] = recentFiles[c];
        recentFiles.length -= 1;
        recentFiles.assumeSafeAppend;
      }
    }
    return false; // don't close menu
  };
}


bool menuKey (KeyEvent event) {
  if (formatWorks != 0) return false;
  if (!inMenu) return false;
  if (inGalaxyMap) return false;
  if (!event.pressed) return false;
  if (currPopup is null) return false;
  auto res = currPopup.onKey(event);
  if (res == PopupMenu.Close) {
    closeMenu();
    freeShipModel();
    refresh();
  } else if (res >= 0) {
    if (onPopupSelect !is null) onPopupSelect(res);
    closeMenu();
    if (popupNoShipKill) popupNoShipKill = false; else freeShipModel();
    refresh();
  } else if (res == PopupMenu.NotMine) {
    if (onPopupKeyEvent !is null && onPopupKeyEvent(event)) {
      closeMenu();
      freeShipModel();
      refresh();
    }
  }
  return true;
}


bool menuChar (dchar dch) {
  if (formatWorks != 0) return false;
  if (!inMenu) return false;
  if (inGalaxyMap) return false;
  if (currPopup is null) return false;
  auto res = currPopup.onChar(dch);
  if (res == PopupMenu.Close) {
    closeMenu();
    freeShipModel();
    refresh();
  } else if (res >= 0) {
    if (onPopupSelect !is null) onPopupSelect(res);
    closeMenu();
    if (popupNoShipKill) popupNoShipKill = false; else freeShipModel();
    refresh();
  }
  return true;
}


bool menuMouse (MouseEvent event) {
  if (formatWorks != 0) return false;
  if (!inMenu) return false;
  if (inGalaxyMap) return false;
  if (currPopup is null) return false;
  auto res = currPopup.onMouse(event);
  if (res == PopupMenu.Close) {
    closeMenu();
    freeShipModel();
    refresh();
  } else if (res >= 0) {
    if (onPopupSelect !is null) onPopupSelect(res);
    closeMenu();
    if (popupNoShipKill) popupNoShipKill = false; else freeShipModel();
    refresh();
  }
  return true;
}


int getCurrHrefIdx () {
  if (hrefcurr < 0 || hrefcurr >= hreflist.length) return -1;
  if (auto hr = hreflist[hrefcurr].widx in bookmeta.hrefs) {
    dstring href = hr.name;
    if (href.length > 1 && href[0] == '#') {
      href = href[1..$];
      foreach (const ref id; bookmeta.ids) if (id.name == href) return id.wordidx;
    }
  }
  return -1;
}


bool readerKey (KeyEvent event) {
  if (formatWorks != 0) return false;
  if (!event.pressed) {
    switch (event.key) {
      case Key.Up: arrowDir = 0; return true;
      case Key.Down: arrowDir = 0; return true;
      default:
    }
    return false;
  }
  switch (event.key) {
    case Key.Space:
      if (event.modifierState&ModifierState.shift) {
        //goto case Key.PageUp;
        hardScrollBy(toMove); toMove = 0;
        scrollBy(-textHeight/3*2);
      } else {
        //goto case Key.PageDown;
        hardScrollBy(toMove); toMove = 0;
        scrollBy(textHeight/3*2);
      }
      break;
    case Key.Backspace:
      popPosition();
      break;
    case Key.PageUp:
      hardScrollBy(toMove); toMove = 0;
      hardScrollBy(-(textHeight > 32 ? textHeight-32 : textHeight));
      break;
    case Key.PageDown:
      hardScrollBy(toMove); toMove = 0;
      hardScrollBy(textHeight > 32 ? textHeight-32 : textHeight);
      break;
    case Key.Up:
      //scrollBy(-8);
      arrowDir = -1;
      break;
    case Key.Down:
      //scrollBy(8);
      arrowDir = 1;
      break;
    case Key.H:
      if (laytext !is null) {
        if (event == "C-H") goEnd(); else goHome();
      }
      break;
    case Key.Tab:
      if (hreflist.length) {
        int dir = (event == "Tab" ? 1 : event == "S-Tab" ? -1 : 0);
        if (dir) {
          if (hrefcurr < 0) {
            hrefcurr = (dir > 0 ? 0 : cast(int)hreflist.length-1);
          } else {
            hrefcurr = (hrefcurr+cast(int)hreflist.length+dir)%cast(int)hreflist.length;
          }
        }
        refresh();
      }
      break;
    case Key.Enter:
      if (hrefcurr >= 0 && event == "Enter") {
        auto wid = getCurrHrefIdx();
        if (wid >= 0) {
          pushPosition();
          goTo(wid);
        }
      }
      break;
    default:
  }
  return true;
}


bool controlKey (KeyEvent event) {
  if (!event.pressed) return false;
  switch (event.key) {
    case Key.Escape:
      if (inGalaxyMap) { inGalaxyMap = false; refresh(); return true; }
      if (inMenu) { closeMenu(); freeShipModel(); refresh(); return true; }
      if (showShip) { showShip = false; freeShipModel(); refresh(); return true; }
      if (hrefcurr >= 0) {
        hrefcurr = -1;
      } else {
        ensureShipModel();
        createQuitMenu(true);
      }
      refresh();
      return true;
    case Key.P: if (event.modifierState == ModifierState.ctrl) { concmd("r_fps toggle"); return true; } break;
    case Key.I: if (event.modifierState == ModifierState.ctrl) { concmd("r_interference toggle"); return true; } break;
    case Key.N: if (event.modifierState == ModifierState.ctrl) { concmd("r_sbleft toggle"); return true; } break;
    case Key.C: if (event.modifierState == ModifierState.ctrl) { if (addIf()) refresh(); return true; } break;
    case Key.E:
      if (event.modifierState == ModifierState.ctrl && eliteShipFiles.length > 0) {
        if (!inMenu) {
          showShip = !showShip;
          if (showShip) ensureShipModel(); else freeShipModel();
          refresh();
        }
        return true;
      }
      break;
    case Key.Q: if (event.modifierState == ModifierState.ctrl) { concmd("quit"); return true; } break;
    case Key.B: if (formatWorks == 0 && !inMenu && !inGalaxyMap && event.modifierState == ModifierState.ctrl) { pushPosition(); return true; } break;
    case Key.S:
      if (formatWorks == 0 && !showShip && !inMenu && !inGalaxyMap) {
        ensureShipModel();
        createSectionMenu();
        refresh();
      }
      break;
    case Key.L:
      if (formatWorks == 0 && event.modifierState == ModifierState.alt) {
        ensureShipModel();
        createRecentMenu();
        refresh();
      }
      break;
    case Key.M:
      if (!inMenu && !showShip) {
        inGalaxyMap = !inGalaxyMap;
        refresh();
      }
      break;
    case Key.R:
      if (formatWorks == 0 && event.modifierState == ModifierState.ctrl) relayout(true);
      break;
    case Key.Home: if (showShip) { setShip(0); return true; } break;
    case Key.End: if (showShip) { setShip(cast(int)eliteShipFiles.length); return true; } break;
    case Key.Up: case Key.Left: if (showShip) { setShip(shipModelIndex-1); return true; } break;
    case Key.Down: case Key.Right: if (showShip) { setShip(shipModelIndex+1); return true; } break;
    default:
  }
  return showShip;
}

int startX () { pragma(inline, true); return (sbLeft ? 2+BND_SCROLLBAR_WIDTH+2 : 4); }
int endX () { pragma(inline, true); return startX+GWidth-4-2-BND_SCROLLBAR_WIDTH-2-1; }

int startY () { pragma(inline, true); return (GHeight-textHeight)/2; }
int endY () { pragma(inline, true); return startY+textHeight-1; }


// ////////////////////////////////////////////////////////////////////////// //
void run () {
  if (GWidth < MinWinWidth) GWidth = MinWinWidth;
  if (GHeight < MinWinHeight) GHeight = MinWinHeight;

  bookText = loadBook(bookFileName);

  setOpenGLContextVersion(3, 0); // it's enough
  //openGLContextCompatible = false;

  sdwindow = new SimpleWindow(GWidth, GHeight, bookText.title~" \xe2\x80\x94 "~bookText.authorFirst~" "~bookText.authorLast, OpenGlOptions.yes, Resizability.allowResizing);
  sdwindow.hideCursor(); // we will do our own
  sdwindow.setMinSize(MinWinWidth, MinWinHeight);

  version(X11) sdwindow.closeQuery = delegate () { concmd("quit"); };

  auto stt = MonoTime.currTime;
  auto prevt = MonoTime.currTime;
  auto curt = prevt;
  textHeight = GHeight-8;

  MonoTime nextIfTime = MonoTime.currTime;

  lastMMove = MonoTime.currTime;

  auto childTid = spawn(&reformatThreadFn, thisTid);
  childTid.setMaxMailboxSize(128, OnCrowding.block);
  thisTid.setMaxMailboxSize(128, OnCrowding.block);

  void loadAndFormat (string filename) {
    assert(formatWorks <= 0);
    bookText = null;
    laytext = null;
    newYLine = -1;
    //formatWorks = -1; //FIXME
    firstFormat = true;
    newYFade = false;
    toMove = 0;
    recentFiles = null;
    arrowDir = 0;
    lastArrowDir = 0;
    arrowSpeed = 0;
    //sdwindow.redrawOpenGlSceneNow();
    //bookText = loadBook(newBookFileName);
    //newBookFileName = null;
    //reformat();
    //if (formatWorks < 0) formatWorks = 1; else ++formatWorks;
    formatWorks = 1;
    //conwriteln("*** loading new book: '", filename, "'");
    hreflist.unsafeArrayClear();
    hrefcurr = -1;
    childTid.send(ReformatWork(null, filename, GWidth, GHeight));
    refresh();
  }

  void reformat () {
    if (formatWorks < 0) formatWorks = 1; else ++formatWorks;
    childTid.send(ReformatWork(cast(shared)bookText, null, GWidth, GHeight));
    refresh();
  }

  void formatComplete (ref ReformatWorkComplete w) {
    scope(exit) { import core.memory : GC; GC.collect(); GC.minimize(); }

    auto lt = cast(LayTextC)w.laytext;
    scope(exit) if (lt) lt.freeMemory();
    w.laytext = null;

    BookMetadata meta = cast(BookMetadata)w.meta;
    w.meta = null;

    BookText bt = cast(BookText)w.booktext;
    w.booktext = null;
    if (bt !is bookText) {
      bookText = bt;
      bookmeta = meta;
      firstFormat = true;
      currentSection = -666;
      setWindowTitle(true);
      //sdwindow.title = bookText.title~" \xe2\x80\x94 "~bookText.authorFirst~" "~bookText.authorLast;
    } else if (bookmeta is null) {
      bookmeta = meta;
    }

    if (isQuitRequested || formatWorks <= 0) return;
    --formatWorks;

    if (w.w != GWidth) {
      if (formatWorks == 0) reformat();
      return;
    }

    if (formatWorks != 0) return;
    freeShipModel();

    uint widx = 0;
    if (!firstFormat && laytext !is null && laytext.lineCount) {
      auto lidx = laytext.findLineAtY(topY);
      if (lidx >= 0) widx = laytext.line(lidx).wstart;
    }
    if (laytext !is null) laytext.freeMemory();
    laytext = lt;
    lt = null;
    if (firstFormat) {
      loadState();
      firstFormat = false;
      doSaveCheck = false;
      stateChanged();
    } else {
      goTo(widx);
    }
    refresh();
  }

  void closeWindow () {
    doSaveState(true); // forced state save
    if (!sdwindow.closed && vg !is null) {
      curImg.clear();
      curImgWhite.clear();
      freeShipModel();
      vg.kill();
      vg = null;
      sdwindow.close();
    }
  }

  sdwindow.visibleForTheFirstTime = delegate () {
    sdwindow.setAsCurrentOpenGlContext(); // make this window active
    sdwindow.vsync = false;
    //sdwindow.useGLFinish = false;
    //glbindLoadFunctions();

    try {
      NVGContextFlag[4] flagList;
      uint flagCount = 0;
      if (flagNanoAA) flagList[flagCount++] = NVGContextFlag.Antialias;
      if (flagNanoSS) flagList[flagCount++] = NVGContextFlag.StencilStrokes;
      if (flagNanoFAA) flagList[flagCount++] = NVGContextFlag.FontAA; else flagList[flagCount++] = NVGContextFlag.FontNoAA;
      vg = nvgCreateContext(flagList[0..flagCount]);
      if (vg is null) {
        conwriteln("Could not init nanovg.");
        assert(0);
        //sdwindow.close();
      }
      loadFonts(vg);
      curImg = createCursorImage(vg);
      curImgWhite = createCursorImage(vg, true);
      fps = new PerfGraph("Frame Time", PerfGraph.Style.FPS, "ui");
    } catch (Exception e) {
      conwriteln("ERROR: ", e.msg);
      concmd("quit");
      return;
    }

    reformat();

    refresh();
    sdwindow.redrawOpenGlScene();
    refresh();
  };

  sdwindow.windowResized = delegate (int w, int h) {
    //conwriteln("w=", w, "; h=", h);
    //if (w < MinWinWidth) w = MinWinWidth;
    //if (h < MinWinHeight) h = MinWinHeight;
    glViewport(0, 0, w, h);
    GWidth = w;
    GHeight = h;
    textHeight = GHeight-8;
    //reformat();
    relayout();
    refresh();
  };

  static bool isWordHref (const ref LayWord w) {
    if (!w.style.href) return false;
    if (auto hr = w.wordNum in bookmeta.hrefs) {
      dstring href = hr.name;
      if (href.length > 1 && href[0] == '#') return true;
    }
    return false;
  }

  static bool isWordHrefByIdx (int widx) {
    if (widx < 0) return false;
    auto w = laytext.wordByIndex(widx);
    if (!w) return false;
    return isWordHref(*w);
  }

  sdwindow.redrawOpenGlScene = delegate () {
    if (isQuitRequested) return;

    glconResize(GWidth/oglConScale, GHeight/oglConScale, oglConScale);
    /*{
      __gshared int cnt;
      conwriteln("cnt=", cnt++);
    }*/

    // update window title
    setWindowTitle();

    //glClearColor(0, 0, 0, 0);
    //glClearColor(0.18, 0.18, 0.18, 0);
    glClearColor(colorBack.r, colorBack.g, colorBack.b, 0);
    glClear(glNVGClearFlags|EliteModel.glClearFlags);

    refreshed();
    needRedrawFlag = (fps !is null && fpsVisible);
    if (vg is null) return;

    scope(exit) vg.releaseImages();
    vg.beginFrame(GWidth, GHeight, 1);
    drawIfs(vg);
    // draw scrollbar
    {
      float curHeight = (laytext !is null ? topY : 0);
      float th = (laytext !is null ? laytext.textHeight-textHeight : 0);
      if (th <= 0) { curHeight = 0; th = 1; }
      float sz = cast(float)(GHeight-4)/th;
      if (sz > 1) sz = 1; else if (sz < 0.05) sz = 0.05;
      int sx = (sbLeft ? 2 : GWidth-BND_SCROLLBAR_WIDTH-2);
      vg.bndScrollSlider(sx, 2, BND_SCROLLBAR_WIDTH, GHeight-4, BND_DEFAULT, curHeight/th, sz);
    }

    int owidx = -1;
    if (laytext is null) {
      if (shipModel is null) {
        vg.beginPath();
        vg.fillColor(colorText);
        int drawY = (GHeight-textHeight)/2;
        vg.intersectScissor((sbLeft ? 2+BND_SCROLLBAR_WIDTH+2 : 4), drawY, GWidth-4-2-BND_SCROLLBAR_WIDTH-2, textHeight);
        vg.fontFaceId(textFont);
        vg.fontSize(fsizeText);
        vg.textAlign(NVGTextAlign.H.Center, NVGTextAlign.V.Middle);
        vg.text(GWidth/2, GHeight/2, "REFORMATTING");
        vg.fill();
      }
    } else if (hrefcurr >= 0) {
      owidx = hreflist[hrefcurr].widx;
    }

    // draw text page
    int markerY = -666;
    hreflist.unsafeArrayClear();
    hrefcurr = -1;
    if (laytext !is null && laytext.lineCount) {
      vg.beginPath();
      vg.fillColor(colorText);
      int drawY = startY;
      vg.intersectScissor((sbLeft ? 2+BND_SCROLLBAR_WIDTH+2 : 4), drawY, GWidth-4-2-BND_SCROLLBAR_WIDTH-2, textHeight);
      //FIXME: not GHeight!
      int lidx = laytext.findLineAtY(topY);
      if (lidx >= 0 && lidx < laytext.lineCount) {
        drawY -= topY-laytext.line(lidx).y;
        vg.textAlign(NVGTextAlign.H.Left, NVGTextAlign.V.Baseline);
        LayFontStyle lastStyle;
        int startx = startX;
        bool setColor = true;
        while (lidx < laytext.lineCount && drawY < GHeight) {
          auto ln = laytext.line(lidx);
          foreach (ref LayWord w; laytext.lineWords(lidx)) {
            if (lastStyle != w.style || setColor) {
              if (w.style.fontface != lastStyle.fontface) vg.fontFace(laytext.fontFace(w.style.fontface));
              vg.fontSize(w.style.fontsize);
              auto c = NVGColor(w.style.color);
              vg.fillColor(c);
              //vg.strokeColor(c);
              lastStyle = w.style;
              setColor = false;
            }
            // line highlighting
            if (newYFade && newYLine == lidx) vg.fillColor(nvgLerpRGBA(colorText, colorTextHi, newYAlpha));
            auto oid = w.objectIdx;
            if (oid >= 0) {
              vg.fill();
              laytext.objectAtIndex(oid).draw(vg, startx+w.x, drawY+ln.h+ln.desc);
              vg.beginPath();
            } else {
              if (laytext.isMarkedWord(w.wordNum)) {
                vg.fill();
                vg.fillColor(nvgRGB(0, 0, 255));
                vg.beginPath();
                vg.rect(startx+w.x, drawY, (laytext.isMarkedWord(w.wordNum+1) ? w.fullwidth :  w.width), w.h);
                vg.fill();
                vg.beginPath();
                vg.fillColor(nvgRGB(255, 255, 255));
                setColor = true;
              }
              vg.text(startx+w.x, drawY+ln.h+ln.desc, laytext.wordText(w));
            }
            // register href
            if (drawY >= 0 && drawY+ln.h <= GHeight && isWordHref(w)) {
              HrefInfo hif;
              hif.widx = cast(int)w.wordNum;
              hif.x0 = startx+w.x;
              hif.y0 = drawY;
              hif.width = w.w;
              hif.height = ln.h;
              hreflist.unsafeArrayAppend(hif);
            }
            //TODO: draw lines over whitespace
            if (lastStyle.underline) vg.rect(startx+w.x, drawY+ln.h+ln.desc+1, w.w, 1);
            if (lastStyle.strike) vg.rect(startx+w.x, drawY+ln.h+ln.desc-w.asc/3, w.w, 2);
            if (lastStyle.overline) vg.rect(startx+w.x, drawY+ln.h+ln.desc-w.asc-1, w.w, 1);
            version(debug_draw) {
              vg.fill();
              vg.beginPath();
              vg.strokeWidth(1);
              vg.strokeColor(nvgRGB(0, 0, 255));
              vg.rect(startx+w.x, drawY, w.w, w.h);
              vg.stroke();
              vg.beginPath();
            }
            if (newYFade && newYLine == lidx) vg.fillColor(NVGColor(lastStyle.color));
          }
          if (newYFade && newYLine == lidx) markerY = drawY;
          drawY += ln.h;
          ++lidx;
        }
      }
      vg.fill();
    }

    // restore current href (if it is possible)
    foreach (auto idx, const ref HrefInfo hif; hreflist) if (hif.widx == owidx) { hrefcurr = cast(int)idx; break; }

    // draw href
    if (hrefcurr >= 0) {
      // background
      vg.beginPath();
      vg.fillColor(nvgRGBA(0, 0, 0, 42));
      vg.rect(hreflist[hrefcurr].x0+0.5f, hreflist[hrefcurr].y0+0.5f, hreflist[hrefcurr].width, hreflist[hrefcurr].height);
      vg.fill();
      // frame
      vg.beginPath();
      vg.strokeWidth(1);
      vg.strokeColor(nvgRGB(0, 0, 255));
      vg.setLineDash([4.0f, 4.0f]);
      vg.rect(hreflist[hrefcurr].x0+0.5f, hreflist[hrefcurr].y0+0.5f, hreflist[hrefcurr].width, hreflist[hrefcurr].height);
      vg.stroke();
      vg.beginPath();
    }

    // draw scroll marker
    if (markerY != -666) {
      vg.beginPath();
      vg.fillColor(NVGColor(0.3f, 0.3f, 0.3f, newYAlpha));
      vg.rect(startX, markerY, GWidth, 2);
      vg.fill();
    }

    // dim text
    if (!showShip) {
      if (colorDim.a != 1) {
        //vg.scissor(0, 0, GWidth, GHeight);
        vg.resetScissor;
        vg.beginPath();
        vg.fillColor(colorDim);
        vg.rect(0, 0, GWidth, GHeight);
        vg.fill();
      }
    }

    // dim more if menu is active
    if (inMenu || showShip || formatWorks != 0) {
      //vg.scissor(0, 0, GWidth, GHeight);
      vg.resetScissor;
      vg.beginPath();
      //vg.globalAlpha(0.5);
      vg.fillColor(nvgRGBA(0, 0, 0, (showShip || formatWorks != 0 ? 127 : 64)));
      vg.rect(0, 0, GWidth, GHeight);
      vg.fill();
    }

    if (shipModel !is null) {
      vg.endFrame();
      float zz = shipModel.bbox[1].z-shipModel.bbox[0].z;
      zz += 10;
      lightsClear();
      /*
      lightAdd(
        0, 60, 60,
        1.0, 0.0, 0.0
      );
      */
      lightAdd(
        //0, 0, -60,
        0, 0, -zz,
        1.0, 1.0, 1.0
      );
      drawModel(shipAngle, shipModel);
      vg.beginFrame(GWidth, GHeight, 1);
      drawShipName();
      //vg.endFrame();
    }

    if (formatWorks == 0) {
      if (inMenu) {
        //vg.beginFrame(GWidth, GHeight, 1);
        //vg.scissor(0, 0, GWidth, GHeight);
        vg.resetScissor;
        currPopup.draw();
        //vg.endFrame();
      }
    }

    if (fps !is null && fpsVisible) {
      //vg.beginFrame(GWidth, GHeight, 1);
      //vg.scissor(0, 0, GWidth, GHeight);
      vg.resetScissor;
      fps.render(vg, GWidth-fps.width-4, GHeight-fps.height-4);
      //vg.endFrame();
    }

    if (inGalaxyMap) drawGalaxy(vg);

    // mouse cursor
    if (curImg.valid && !mouseHidden) {
      int w, h;
      //vg.beginFrame(GWidth, GHeight, 1);
      vg.beginPath();
      //vg.scissor(0, 0, GWidth, GHeight);
      vg.resetScissor;
      vg.imageSize(curImg, w, h);
      if (mouseFadingAway) {
        mouseAlpha -= 0.1;
        if (mouseAlpha <= 0) { mouseFadingAway = false; mouseHidden = true; }
      } else {
        mouseAlpha = 1.0f;
      }
      vg.globalAlpha(mouseAlpha);
      if (!mouseHigh) {
        vg.fillPaint(vg.imagePattern(mouseX, mouseY, w, h, 0, curImg, 1));
      } else {
        vg.fillPaint(vg.imagePattern(mouseX, mouseY, w, h, 0, curImgWhite, 1));
      }
      vg.rect(mouseX, mouseY, w, h);
      vg.fill();
      /*
      if (mouseHigh) {
        vg.beginPath();
        vg.fillPaint(vg.imagePattern(mouseX, mouseY, w, h, 0, curImgWhite, 0.4));
        vg.rect(mouseX, mouseY, w, h);
        vg.fill();
      }
      */
      //vg.endFrame();
    }
    vg.endFrame();
    glconDraw();
  };

  void processThreads () {
    ReformatWorkComplete wd;
    for (;;) {
      bool workDone = false;
      auto res = receiveTimeout(Duration.zero,
        (QuitWork w) {
          formatWorks = -1;
        },
        (ReformatWorkComplete w) {
          wd = w;
          workDone = true;
        },
      );
      if (!res) { assert(!workDone); break; }
      if (workDone) { workDone = false; formatComplete(wd); }
    }
  }

  auto lastTimerEventTime = MonoTime.currTime;
  bool somethingVisible = true;

  sdwindow.visibilityChanged = delegate (bool vis) {
    //import core.stdc.stdio; printf("VISCHANGED: %s\n", (vis ? "tan" : "ona").ptr);
    somethingVisible = vis;
  };

  conRegVar!fpsVisible("r_fps", "show fps indicator", (self, valstr) { refresh(); });
  conRegVar!inGalaxyMap("r_galaxymap", "show Elite galaxy map", (self, valstr) { refresh(); });

  conRegVar!interAllowed("r_interference", "show interference", (self, valstr) { refresh(); });
  conRegVar!sbLeft("r_sbleft", "show scrollbar at the left side", (self, valstr) { refresh(); });

  conRegVar!showShip("r_showship", "show Elite ship", (self, valstr) {
      if (eliteShipFiles.length == 0) return false;
      return true;
    },
    (self, valstr) {
      if (showShip) ensureShipModel(); else freeShipModel();
      refresh();
    },
  );

  conRegFunc!(() {
    if (currPopup !is null) {
      currPopup.destroy;
      currPopup = null;
      freeShipModel();
      refresh();
    }
    onPopupSelect = null;
  })("menu_close", "close current popup menu");

  conRegFunc!(() {
    if (formatWorks == 0 && !showShip && !inGalaxyMap) {
      currPopup.destroy;
      currPopup = null;
      ensureShipModel();
      createSectionMenu();
      refresh();
    }
  })("menu_section", "show section menu");

  conRegFunc!(() {
    if (formatWorks == 0 && !showShip && !inGalaxyMap) {
      currPopup.destroy;
      currPopup = null;
      ensureShipModel();
      createRecentMenu();
      refresh();
    }
  })("menu_recent", "show recent menu");

  sdwindow.eventLoop(1000/34,
    delegate () {
      processThreads();
      if (sdwindow.closed) return;
      conProcessQueue();
      if (isQuitRequested) { closeWindow(); return; }
      auto ctt = MonoTime.currTime;

      {
        auto spass = (ctt-lastTimerEventTime).total!"msecs";
        //if (spass >= 30) { import core.stdc.stdio; printf("WARNING: too long frame time: %u\n", cast(uint)spass); }
        //{ import core.stdc.stdio; printf("FRAME TIME: %u\n", cast(uint)spass); }
        lastTimerEventTime = ctt;
        // update FPS timer
        prevt = curt;
        //curt = MonoTime.currTime;
        curt = ctt;
        //auto secs = cast(double)((curt-stt).total!"msecs")/1000.0;
        auto dt = cast(double)((curt-prevt).total!"msecs")/1000.0;
        if (fps !is null) fps.update(dt);
      }

      // smooth scrolling
      if (formatWorks == 0) {
        if (!lastArrowDir) lastArrowDir = arrowDir;
        if (arrowDir && arrowDir == lastArrowDir) {
          arrowSpeed += 0.015;
          if (arrowSpeed >= 1) arrowSpeed = 1;
        } else if (arrowDir) {
          assert(lastArrowDir != arrowDir);
          lastArrowDir = arrowDir;
          arrowSpeed *= 0.6;
        } else {
          arrowSpeed -= 0.025; //*(lastArrowDir ? 4 : 1);
          if (arrowSpeed <= 0) {
            arrowSpeed = 0;
            if (arrowDir) {
              lastArrowDir = arrowDir;
            }
          }
        }
        if (toMove != 0) {
          import std.math : abs;
          enum Delta = 92*2;
          immutable int sign = (toMove < 0 ? -1 : 1);
          // change speed
          static int arrowSpeed = 0;
          if (arrowSpeed == 0) arrowSpeed = 16;
          if (abs(toMove) <= arrowSpeed) {
            arrowSpeed /= 2;
            if (arrowSpeed < 4) arrowSpeed = 4;
          } else {
            arrowSpeed *= 2;
            if (arrowSpeed > Delta) arrowSpeed = Delta;
          }
          // calc move distance
          int sc = arrowSpeed;
          if (sc > abs(toMove)) sc = abs(toMove);
          hardScrollBy(sc*sign);
          toMove -= sc*sign;
          if (toMove == 0) arrowSpeed = 0;
          nextFadeTime = ctt+500.msecs;
          refresh();
        }
        {
          int toMove = cast(int)(pulse.pulse(arrowSpeed)*64);
          //if (arrowSpeed) { import std.stdio; writeln("lastdir=", lastArrowDir, "; dir=", arrowDir, "; as=", arrowSpeed, "; toMove=", toMove); }
          if (toMove > 1) {
            toMove *= lastArrowDir;
            hardScrollBy(toMove);
            nextFadeTime = ctt+500.msecs;
            refresh();
          }
        }
        /*
        enum Delta = 92*2;
        if (toMove != 0) {
          import std.math : abs;
          immutable int sign = (toMove < 0 ? -1 : 1);
          // change speed
          if (arrowSpeed == 0) arrowSpeed = 16;
          if (abs(toMove) <= arrowSpeed) {
            arrowSpeed /= 2;
            if (arrowSpeed < 4) arrowSpeed = 4;
          } else {
            arrowSpeed *= 2;
            if (arrowSpeed > Delta) arrowSpeed = Delta;
          }
          // calc move distance
          int sc = arrowSpeed;
          if (sc > abs(toMove)) sc = abs(toMove);
          hardScrollBy(sc*sign);
          toMove -= sc*sign;
          if (toMove == 0) arrowSpeed = 0;
          nextFadeTime = ctt+500.msecs;
          refresh();
        } else if (arrowDir) {
          if ((arrowDir < 0 && arrowSpeed > 0) || (arrowDir > 0 && arrowSpeed < 0)) arrowSpeed += arrowDir*4;
          arrowSpeed += arrowDir*2;
          if (arrowSpeed < -64) arrowSpeed = -64; else if (arrowSpeed > 64) arrowSpeed = 64;
          hardScrollBy(arrowSpeed);
          refresh();
        } else if (arrowSpeed != 0) {
          if (arrowSpeed < 0) {
            if ((arrowSpeed += 4) > 0) arrowSpeed = 0;
          } else {
            if ((arrowSpeed -= 4) < 0) arrowSpeed = 0;
          }
          if (arrowSpeed) {
            hardScrollBy(arrowSpeed);
            refresh();
          }
        }
        */
        // highlight fading
        if (newYFade) {
          if (ctt >= nextFadeTime) {
            if ((newYAlpha -= 0.1) <= 0) {
              newYFade = false;
            } else {
              nextFadeTime = ctt+25.msecs;
            }
            refresh();
          }
        }
      }
      // interference processing
      if (ctt >= nextIfTime) {
        import std.random : uniform;
        if (uniform!"[]"(0, 100) >= 50) { if (addIf()) refresh(); }
        nextIfTime += (uniform!"[]"(50, 1500)).msecs;
      }
      if (processIfs()) refresh();
      // ship rotation
      if (shipModel !is null) {
        shipAngle -= 1;
        if (shipAngle < 359) shipAngle += 360;
        refresh();
      }
      // mouse autohide
      if (!mouseFadingAway) {
        if (!mouseHidden && !mouseHigh) {
          if ((ctt-lastMMove).total!"msecs" > 2500) {
            //mouseHidden = true;
            mouseFadingAway = true;
            mouseAlpha = 1.0f;
            refresh();
          }
        }
      }
      if (somethingVisible) {
        // sadly, to keep framerate we have to redraw each frame, or driver will throw us out of the ship
        if (/*needRedraw*/true) sdwindow.redrawOpenGlSceneNow();
      } else {
        refresh();
      }
      doSaveState();
      // load new book?
      if (newBookFileName.length && formatWorks == 0 && vg !is null) {
        doSaveState(true); // forced state save
        closeMenu();
        ensureShipModel();
        loadAndFormat(newBookFileName);
        newBookFileName = null;
        sdwindow.redrawOpenGlSceneNow();
        //refresh();
      }
    },
    delegate (KeyEvent event) {
      if (sdwindow.closed) return;
      if (glconKeyEvent(event)) return;
      if (event.key == Key.PadEnter) event.key = Key.Enter;
      if ((event.modifierState&ModifierState.numLock) == 0) {
        switch (event.key) {
          case Key.Pad0: event.key = Key.Insert; break;
          case Key.PadDot: event.key = Key.Delete; break;
          case Key.Pad1: event.key = Key.End; break;
          case Key.Pad2: event.key = Key.Down; break;
          case Key.Pad3: event.key = Key.PageDown; break;
          case Key.Pad4: event.key = Key.Left; break;
          case Key.Pad6: event.key = Key.Right; break;
          case Key.Pad7: event.key = Key.Home; break;
          case Key.Pad8: event.key = Key.Up; break;
          case Key.Pad9: event.key = Key.PageUp; break;
          //case Key.PadEnter: event.key = Key.Enter; break;
          default:
        }
      }
      if (controlKey(event)) return;
      if (menuKey(event)) return;
      if (readerKey(event)) return;
      if (((event.modifierState&ModifierState.ctrl) != 0 && event.key == Key.C) ||
          ((event.modifierState&ModifierState.ctrl) != 0 && event.key == Key.Insert))
      {
        string ltext = laytext.getMarkedText();
        if (ltext.length) {
          setClipboardText(sdwindow, ltext);
          setPrimarySelection(sdwindow, ltext);
          setSecondarySelection(sdwindow, ltext);
        }
      }
    },

    delegate (MouseEvent event) {
      if (sdwindow.closed) return;

      int linkAt (int msx, int msy) {
        if (laytext !is null && bookmeta !is null) {
          if (msx >= startX && msx <= endX && msy >= startY && msy <= endY) {
            auto widx = laytext.wordAtXY(msx-startX, topY+msy-startY);
            if (widx >= 0) {
              //conwriteln("word at (", msx-startX, ",", msy-startY, "): ", widx);
              auto w = laytext.wordByIndex(widx);
              while (widx >= 0) {
                //conwriteln("word #", widx, "; href=", w.style.href);
                if (!w.style.href) break;
                if (auto hr = w.wordNum in bookmeta.hrefs) {
                  dstring href = hr.name;
                  if (href.length > 1 && href[0] == '#') {
                    href = href[1..$];
                    foreach (const ref id; bookmeta.ids) {
                      if (id.name == href) {
                        //pushPosition();
                        //goTo(id.wordidx);
                        return id.wordidx;
                      }
                    }
                    //conwriteln("id '", hr.name, "' not found!");
                    return -1;
                  }
                }
                --widx;
                --w;
              }
            }
          }
        }
        return -1;
      }

      lastMMove = MonoTime.currTime;
      if (mouseHidden || mouseFadingAway) {
        mouseHidden = false;
        mouseFadingAway = false;
        mouseAlpha = 1.0f;
        refresh();
      }
      if (mouseX != event.x || mouseY != event.y) {
        mouseX = event.x;
        mouseY = event.y;
        refresh();
      }
      if (!menuMouse(event) && !showShip) {
        if (mouseStartMarking && event.type == MouseEventType.motion) {
          if (laytext && event.x >= startX && event.x <= endX &&
              event.y >= startY && event.y <= endY)
          {
            laytext.setMark(laytext.MarkType.End, event.x-startX, topY+event.y-startY);
            refresh();
          }
        }
        if (event.type == MouseEventType.buttonPressed) {
          switch (event.button) {
            case MouseButton.wheelUp: hardScrollBy(-42); break;
            case MouseButton.wheelDown: hardScrollBy(42); break;
            case MouseButton.left:
              auto wid = linkAt(event.x, event.y);
              if (wid >= 0) {
                pushPosition();
                goTo(wid);
              } else {
                if (laytext && event.x >= startX && event.x <= endX &&
                    event.y >= startY && event.y <= endY)
                {
                  mouseStartMarking = true;
                  laytext.setMark(laytext.MarkType.Both, event.x-startX, topY+event.y-startY);
                  refresh();
                }
              }
              break;
            case MouseButton.right:
              popPosition();
              break;
            default:
          }
        } else if (event.type == MouseEventType.buttonReleased) {
          if (event.button == MouseButton.left) {
            mouseStartMarking = false;
          }
        }
      } else {
        mouseStartMarking = false;
      }
      mouseHigh = (linkAt(event.x, event.y) >= 0);
    },
    delegate (dchar ch) {
      if (sdwindow.closed) return;
      if (glconCharEvent(ch)) return;
      if (menuChar(ch)) return;
      //if (ch == '`') { concmd("r_console tan"); return; }
    },
  );
  closeWindow();

  childTid.send(QuitWork());
  while (formatWorks >= 0) processThreads();
}


// ////////////////////////////////////////////////////////////////////////// //
struct FlibustaUrl {
  string fullUrl; // onion
  string host;
  string id;

  this (const(char)[] aurl) {
    import std.format : format;
    aurl = aurl.xstrip();
    auto flibustaRE = regex(`^(?:https?://)?(?:(?:(?:www\.)?flibusta\.[^/]+)|(?:flibusta[^.]*.onion))/b/(\d+)`);
    auto ct = aurl.matchFirst(flibustaRE);
    if (!ct.empty) {
      fullUrl = "http://flibustaongezhld6dibs2dps6vm4nvqg2kp7vgowbu76tzopgnhazqd.onion/b/%s/fb2".format(ct[1]);
      id = ct[1].idup;
      host = "flibustaongezhld6dibs2dps6vm4nvqg2kp7vgowbu76tzopgnhazqd.onion";
    } else {
      // add protocol
      auto protoRE = regex(`^([^:/]+):`);
      auto protoMt = aurl.matchFirst(protoRE);
      if (protoMt.empty) fullUrl = "http:%s%s".format((aurl[0] == '/' ? "" : "//"), aurl);
      // add host
      auto hostRE = regex(`^(?:[^:/]+)://([^/]+)`);
      auto hostMt = fullUrl.matchFirst(hostRE);
      if (hostMt.empty) { fullUrl = null; return; }
      host = hostMt[1].idup;
    }
  }

  @property bool valid () const pure nothrow @safe @nogc { pragma(inline, true); return (fullUrl.length > 0); }
  @property bool isFlibusta () const pure nothrow @safe @nogc { pragma(inline, true); return (id.length > 0); }
  @property bool isOnion () const pure nothrow @safe @nogc { pragma(inline, true); return host.endsWithCI(".onion"); }
}


// ////////////////////////////////////////////////////////////////////////// //
string dbFindInCache() (in auto ref FlibustaUrl furl) {
  if (!furl.valid) return null;
  auto stmt = filedb.statement(`
    SELECT filename AS filename
    FROM flubusta_cache
    WHERE flibusta_id=:flid
    LIMIT 1
  `);

  string fname;
  foreach (auto row; stmt.bindText(":flid", furl.id).range) {
    fname = row.filename!string;
  }

  if (fname.length == 0) return null;

  import std.file : exists;
  try {
    if (fname.exists) return fname;
  } catch (Exception e) {}

  // no disk file, delete from cache
  stmt = filedb.statement(`
    DELETE FROM flubusta_cache
    WHERE flibusta_id=:flid
  `);
  stmt.bindText(":flid", furl.id).doAll();

  return null;
}


void dbPutToCache() (in auto ref FlibustaUrl furl, const(char)[] fname) {
  if (!furl.valid || fname.length == 0 || furl.id.length == 0) return;

  auto stmt = filedb.statement(`
    INSERT INTO flubusta_cache
            ( flibusta_id, filename)
      VALUES(:flibusta_id,:filename)
    ON CONFLICT(flibusta_id)
      DO UPDATE
      SET filename=:filename
  `);
  stmt
    .bindText(":flibusta_id", furl.id)
    .bindText(":filename", fname)
    .doAll();
}


// ////////////////////////////////////////////////////////////////////////// //
// returns file name
string fileDown() (in auto ref FlibustaUrl furl) {
  if (!furl.valid || !furl.isFlibusta) return null;

  string cachedFName = dbFindInCache(furl);
  if (cachedFName.length) return cachedFName;

  // content-disposition: attachment; filename="Divov_Sled-zombi.1lzb6Q.96382.fb2.zip"
  auto cdRE0 = regex(`^\s*attachment\s*;\s*filename="(.+?)"`, "i");
  auto cdRE1 = regex(`^\s*attachment\s*;\s*filename=([^;]+?)`, "i");
  auto cdLoc0 = regex(`.*/b\.fb2/([^/]+?\.fb2\.zip)$`, "i");

  auto http = HTTP(furl.host);
  http.method = HTTP.Method.get;
  http.url = furl.fullUrl;

  string fname = null;
  string tmpfname = null;
  string fnps = null;
  bool alreadyDowned = false;
  VFile fo;

  http.onReceiveHeader = delegate (in char[] key, in char[] value) {
    //writeln(key ~ ": " ~ value);
    if (key.strEquCI("content-disposition")) {
      auto ct = value.matchFirst(cdRE0);
      if (ct.empty) ct = value.matchFirst(cdRE1);
      if (ct[1].length) {
        auto fnp = ct[1].xstrip;
        auto lslpos = fnp.lastIndexOf('/');
        if (lslpos > 0) fnp = fnp[lslpos+1..$];
        if (fnp.length == 0) {
          fname = null;
        } else {
          import std.file : exists, mkdirRecurse;
          import std.path;
          string xfn = buildPath(RcDir, "cache");
          xfn.mkdirRecurse();
          char[] xxname;
          xxname.reserve(fnp.length);
          foreach (char ch; fnp) {
            if (ch <= ' ' || ch == 127) ch = '_';
            xxname ~= ch;
          }
          fnps = cast(string)xxname; // it is safe to cast here
          fname = buildPath(xfn, fnps);
          tmpfname = fname~".down.part";
          /*
          if (fname.exists) {
            alreadyDowned = true;
            throw new Exception("already here");
            //throw new FileAlreadyDowned("already here");
          }
          */
          //write("\r", fnp, " [", furl.fullUrl, "]\e[K");
        }
      }
    } else if (key.strEquCI("location")) {
      auto mt = value.matchFirst(cdLoc0);
      if (!mt.empty && mt[1].length > 0) {
        import std.file : exists, mkdirRecurse;
        import std.path;
        auto fnp = mt[1].xstrip;
        string xfn = buildPath(RcDir, "cache");
        xfn.mkdirRecurse();
        char[] xxname;
        xxname.reserve(fnp.length);
        foreach (char ch; fnp) {
          if (ch <= ' ' || ch == 127) ch = '_';
          xxname ~= ch;
        }
        fnps = cast(string)xxname; // it is safe to cast here
        fname = buildPath(xfn, fnps);
        tmpfname = fname~".down.part";
        /*
        if (fname.exists) {
          alreadyDowned = true;
          throw new Exception("already here");
          //throw new FileAlreadyDowned("already here");
        }
        */
        //write("\r", fnp, " [", furl.fullUrl, "]\e[K");
      }
    }
  };

  http.onReceive = delegate (ubyte[] data) {
    if (!fo.isOpen) {
      if (fname.length == 0) throw new Exception("no file name found in headers");
      //writeln("  downloading to ", fname);
      fo = VFile(tmpfname, "w");
    }
    fo.rawWriteExact(data);
    return data.length;
  };

  MonoTime lastProgTime = MonoTime.zero;
  enum BarLength = 68;
  bool doProgUpdate = true;
  char[1024] buf = void;
  int oldDots = -1, oldPrc = -1;
  uint bufpos = 0;
  int stickPos = 1;
  immutable string stickStr = `|/-\`;

  // will set `doProgUpdate`, and update `oldXXX`
  void buildPBar (usize dlTotal, usize dlNow) {
    void put (const(char)[] s...) nothrow {
      if (s.length == 0) return;
      if (bufpos >= buf.length) return;
      int left = cast(int)buf.length-bufpos;
      if (s.length > left) s = s[0..left];
      assert(s.length > 0);
      import core.stdc.string : memcpy;
      memcpy(buf.ptr+bufpos, s.ptr, s.length);
      bufpos += cast(int)s.length;
    }
    void putprc (int prc) {
      if (prc < 0) prc = 0; else if (prc > 100) prc = 100;
      if (bufpos >= buf.length || buf.length-bufpos < 5) return; // oops
      import core.stdc.stdio;
      bufpos += cast(int)snprintf(buf.ptr+bufpos, 5, "%3d%%", prc);
    }
    void putCommaNum (usize n, usize max=0) {
      char[128] buf = void;
      if (max < n) {
        put(intWithCommas(buf[], n));
      } else {
        auto len = intWithCommas(buf[], max).length;
        auto pt = intWithCommas(buf[], n);
        while (len-- > pt.length) put(" ");
        put(pt);
      }
    }
    bufpos = 0;
    put("\r");
    put(fnps);
    put(" [");
    auto barpos = bufpos;
    foreach (immutable _; 0..BarLength) put(" ");
    put("]");
    if (dlTotal > 0) {
      int prc = cast(int)(cast(ulong)100*dlNow/dlTotal);
      if (prc < 0) prc = 0; else if (prc > 100) prc = 100;
      int dots = cast(int)(cast(ulong)BarLength*dlNow/dlTotal);
      if (dots < 0) dots = 0; else if (dots > BarLength) dots = BarLength;
      if (prc != oldPrc || dots != oldDots) {
        doProgUpdate = true;
        oldPrc = prc;
        oldDots = dots;
      }
      put(" [");
      putCommaNum(dlNow, dlTotal);
      put("/");
      putCommaNum(dlTotal);
      put("] ");
      putprc(prc);
      // dots
      foreach (immutable dp; 0..dots) if (barpos+dp < buf.length) buf[barpos+dp] = '.';
    } else {
      put("?\e[K");
      if (oldDots != -1 || oldPrc != -1) doProgUpdate = true;
      oldDots = -1;
      oldPrc = -1;
    }
  }

  http.onProgress = delegate (usize dltotal, usize dlnow, usize ultotal, usize ulnow) {
    //writeln("Progress ", dltotal, ", ", dlnow, ", ", ultotal, ", ", ulnow);
    if (fname.length == 0) {
      auto ct = MonoTime.currTime;
      if ((ct-lastProgTime).total!"msecs" >= 100) {
        write("\x08", stickStr[stickPos]);
        stickPos = (stickPos+1)%cast(int)stickStr.length;
        lastProgTime = ct;
      }
      return 0;
    }
    buildPBar(dltotal, dlnow);
    if (doProgUpdate) { write("\e[?7l", buf[0..bufpos], "\e[K\e[?7h"); doProgUpdate = false; }
    //if (dltotal == 0) return 0;
    //auto ct = MonoTime.currTime;
    //if ((ct-lastProgTime).total!"msecs" < 1000) return 0;
    //lastProgTime = ct;
    //writef("\r%s [%s] -- [%s/%s] %3u%%\e[K", fnps, host, intWithCommas(dlnow), intWithCommas(dltotal), 100UL*dlnow/dltotal);
    return 0;
  };

  if (furl.isOnion) {
    http.proxyType = HTTP.CurlProxy.socks5_hostname;
    http.proxy = "127.0.0.1";
    http.proxyPort = 9050;
  }

  try {
    //write("downloading from [", host, "]: ", realUrl, " ... ");
    write("downloading from Flibusta: ", furl.fullUrl, " ... ", stickStr[0]);
    http.perform();
    if (fo.isOpen) {
      buildPBar(cast(uint)fo.size, cast(uint)fo.size);
    } else {
      buildPBar(1, 1);
    }
    writeln(buf[0..bufpos], "\e[K");
    //write("\r\e[K");
  } catch (Exception e) {
    if (/*cast(FileAlreadyDowned)e*/alreadyDowned) {
      write("\r", fname, " already downloaded.\e[K");
      return fname; // already here
    }
    if (tmpfname.length) {
      import std.exception : collectException;
      import std.file : remove;
      collectException(tmpfname.remove);
    }
    throw e;
  }

  if (fo.isOpen) {
    // something was downloaded, rename it
    import std.file : rename;
    fo.close();
    rename(tmpfname, fname);
    dbPutToCache(furl, fname);
    return fname;
  }

  return null;
}


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  import std.path;

  conRegVar!oglConScale(1, 4, "r_conscale", "console scale");

  conProcessQueue(256*1024); // load config
  conProcessArgs!true(args);
  conProcessQueue(256*1024);

  universe = Galaxy(0);

  if (args.length == 1) {
    string lnn = getLatestFileName();
    if (lnn.length) args ~= lnn;
  } else {
    if (args.length != 2) assert(0, "invalid number of arguments");
    auto furl = FlibustaUrl(args[1]);
    if (furl.valid && furl.isFlibusta) {
      string fn = fileDown(furl);
      if (fn.length == 0) assert(0, "can't download file");
      args[1] = fn;
    }
  }

  if (args.length == 1) assert(0, "no filename");

  readConfig();

  bookFileName = args[1];
  run();
}
