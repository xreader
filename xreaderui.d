/* Written by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module xreaderui;

import arsd.simpledisplay;
import arsd.image;

import iv.nanovega;
import iv.nanovega.blendish;
import iv.strex;
import iv.utfutil;
import iv.vfs;
import iv.vfs.io;

import xreadercfg;


// ////////////////////////////////////////////////////////////////////////// //
void loadFonts (NVGContext vg) {
  void loadFont (ref int fid, string name, string path) {
    fid = vg.createFont(name, path);
    if (fid < 0) throw new Exception("can't load font '"~name~"' from '"~path~"'");
    //writeln("id=", fid, "; name=<", name, ">; path=", path);
  }

  loadFont(textFont, "text", textFontName);
  loadFont(textiFont, "texti", textiFontName);
  loadFont(textbFont, "textb", textbFontName);
  loadFont(textzFont, "textz", textzFontName);

  loadFont(monoFont, "mono", monoFontName);
  loadFont(monoiFont, "monoi", monoiFontName);
  loadFont(monobFont, "monob", monobFontName);
  loadFont(monozFont, "monoz", monozFontName);

  loadFont(uiFont, "ui", uiFontName);

  loadFont(galmapFont, "galmap", galmapFontName);

  bndSetFont(uiFont);
}


// ////////////////////////////////////////////////////////////////////////// //
// cursor (hi, Death Track!)
private enum curWidth = 17;
private enum curHeight = 23;
private static immutable ubyte[curWidth*curHeight] curImg = [
  0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,3,2,0,0,0,0,0,0,0,0,0,0,0,0,0,
  1,0,3,2,2,0,0,0,0,0,0,0,0,0,0,0,0,
  1,1,3,3,2,2,0,0,0,0,0,0,0,0,0,0,0,
  1,1,3,3,4,2,2,0,0,0,0,0,0,0,0,0,0,
  1,1,3,3,4,4,2,2,0,0,0,0,0,0,0,0,0,
  1,1,3,3,4,4,4,2,2,0,0,0,0,0,0,0,0,
  1,1,3,3,4,4,4,4,2,2,0,0,0,0,0,0,0,
  1,1,3,3,4,4,4,5,6,2,2,0,0,0,0,0,0,
  1,1,3,3,4,4,5,6,7,5,2,2,0,0,0,0,0,
  1,1,3,3,4,5,6,7,5,4,5,2,2,0,0,0,0,
  1,1,3,3,5,6,7,5,4,5,6,7,2,2,0,0,0,
  1,1,3,3,6,7,5,4,5,6,7,7,7,2,2,0,0,
  1,1,3,3,7,5,4,5,6,7,7,7,7,7,2,2,0,
  1,1,3,3,5,4,5,6,8,8,8,8,8,8,8,8,2,
  1,1,3,3,4,5,6,3,8,8,8,8,8,8,8,8,8,
  1,1,3,3,5,6,3,3,1,1,1,1,1,1,1,0,0,
  1,1,3,3,6,3,3,1,1,1,1,1,1,1,1,0,0,
  1,1,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,
  1,1,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,
  1,1,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,
  1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
];
struct CC { ubyte r, g, b, a; }
private static immutable CC[9] curPal = [
  CC(  0,  0,  0,  0),
  CC(  0,  0,  0,127),
  CC( 85,255,255,255),
  CC( 85, 85,255,255),
  CC(255, 85, 85,255),
  CC(170,  0,170,255),
  CC( 85, 85, 85,255),
  CC(  0,  0,  0,255),
  CC(  0,  0,170,255),
];


NVGImage createCursorImage (NVGContext vg, bool white=false) {
  auto img = new ubyte[](curWidth*curHeight*4);
  scope(exit) img.destroy;
  const(ubyte)* s = curImg.ptr;
  auto d = img.ptr;
  foreach (immutable _; 0..curWidth*curHeight) {
    CC c = curPal.ptr[*s++];
    if (white) {
      if (c.a == 255) {
        //c = CC(255, 127, 0, 255);
        auto hsl = nvgRGB(c.r, c.g, c.b).asHSL;
        //hsl.l *= 1.25; if (hsl.l > 0.8) hsl.l = 0.8;
        //hsl.l *= 1.25; if (hsl.l > 1) hsl.l = 1;
        //hsl.h *= 2.25; if (hsl.h > 1) hsl.h = 1;
        hsl.h *= 0.1;
        auto cc = hsl.asColor.asUint;
        *d++ = cc&0xff; cc >>= 8;
        *d++ = cc&0xff; cc >>= 8;
        *d++ = cc&0xff; cc >>= 8;
        *d++ = cc&0xff; cc >>= 8;
      } else {
        *d++ = c.r;
        *d++ = c.g;
        *d++ = c.b;
        *d++ = c.a;
      }
    } else {
      *d++ = c.r;
      *d++ = c.g;
      *d++ = c.b;
      *d++ = c.a;
    }
  }
  return vg.createImageRGBA(curWidth, curHeight, img[]);
}


// ////////////////////////////////////////////////////////////////////////// //
final class PopupMenu {
  //enum LeftX = 10;
  //enum TopY = 10;
  enum Padding = 8;

  static struct SecInfo {
    int w, h;
    dstring s;
    string skoi;

    void buildKoi () nothrow {
      skoi = null;
      foreach (dchar dc; s) skoi ~= uni2koi(dc);
    }

    private static bool xmatch (const(char)[] s, const(char)[] pat) nothrow @trusted @nogc {
      if (s.length == 0) return (pat.length == 0);
      while (s.length && pat.length) {
        // is pattern char space?
        if (pat[0] <= ' ') {
          if (s[0] > ' ') return false;
          pat = pat.xstrip;
          s = s.xstrip;
          continue;
        }
        // pattern char is not space
        if (koi8lower(s[0]) != koi8lower(pat[0])) return false;
        s = s[1..$];
        pat = pat[1..$];
      }
      return (pat.length == 0);
    }

    bool filterMatch (const(char)[] flt) const nothrow @nogc {
      if (flt.length == 0) return true;
      //if (skoi.length < flt) return false;
      foreach (immutable dpos; 0..skoi.length-1) {
        if (xmatch(skoi[dpos..$], flt)) return true;
      }
      return false;
    }
  }

  static struct VisItem {
    int idx; // item index in `items`
  }

  bool hasTitle;
  SecInfo menuTitle;
  private SecInfo[] items;
  private VisItem[] visitems;
  private int curItemIdx = 0;
  private int topItemIdx = 0;
  private int hoverItem = -1;
  NVGContext vg;
  int maxW; // maximum text width
  bool needRedraw;
  int leftX, topY;
  bool moving;

  private char[] filter; // WARNING! it is unsafe to slice this!

  bool allowFiltering = false;

  void textSize (dstring s, out int tw, out int th) {
    auto w = vg.bndLabelWidth(-1, s)+4;
    //{ import core.stdc.stdio : printf; printf("* 03: w=%g\n", cast(double)w); }
    if (w > GWidth-Padding*4) w = GWidth-Padding*4;
    //{ import core.stdc.stdio : printf; printf("* 04: w=%g\n", cast(double)w); }
    auto h = vg.bndLabelHeight(-1, s, w);
    if (h < BND_WIDGET_HEIGHT) h = BND_WIDGET_HEIGHT;
    tw = cast(int)w;
    th = cast(int)h;
  }

  this (NVGContext avg, dstring atitle, scope dstring[] delegate () buildSections) {
    assert(avg !is null);
    //writeln("fw=", avg.width, "; pr=", avg.devicePixelRatio);
    bool doEndFrame = !avg.inFrame;
    if (doEndFrame) avg.beginFrame(1024, 768);
    scope(exit) if (doEndFrame) avg.cancelFrame();
    vg = avg;
    vg.fontFaceId = uiFont;
    vg.textAlign(NVGTextAlign.H.Left, NVGTextAlign.V.Baseline);
    vg.fontSize = fsizeUI;
    maxW = 0;
    if (atitle !is null) {
      hasTitle = true;
      items.length += 1;
      items[0].s = atitle;
      textSize(atitle, items[0].w, items[0].h);
      maxW = items[0].w;
    }
    if (buildSections !is null) {
      auto list = buildSections();
      auto cpos = items.length;
      items.length += list.length;
      foreach (dstring s; list) {
        items[cpos].s = s;
        items[cpos].buildKoi();
        textSize(s, items[cpos].w, items[cpos].h);
        if (maxW < items[cpos].w) maxW = items[cpos].w;
        ++cpos;
      }
    }
    if (hasTitle) {
      menuTitle = items[0];
      items = items[1..$];
    }
    //writeln("maxW=", maxW);
    leftX = (GWidth-maxW-Padding*4)/2;
    topY = Padding;
    int ey = calcBotY;
    //topY = (GHeight-(ey-topY))/2;
    topY = Padding;
    refilter();
    needRedraw = true;
  }

  private ref SecInfo itAt (int idx) nothrow @nogc {
    if (idx < 0 || idx >= visitems.length) assert(0, "ui internal error");
    return items[visitems[idx].idx];
  }

  @property bool isCurValid () const pure nothrow @safe @nogc { pragma(inline, true); return (curItemIdx >= 0 && curItemIdx < visitems.length); }

  @property int itemIndex () const pure nothrow @safe @nogc { pragma(inline, true); return (curItemIdx >= 0 && curItemIdx < visitems.length ? visitems[curItemIdx].idx : -1); }

  @property void itemIndex (int v) nothrow @trusted @nogc {
         if (items.length == 0) curItemIdx = 0;
    else if (v < 0) v = 0;
    else if (v >= items.length) v = cast(int)(items.length-1);
    foreach (immutable vidx, const ref vi; visitems) {
      if (vi.idx == v) { curItemIdx = cast(int)vidx; return; }
    }
    //curItemIdx = 0;
  }

  void refilter () {
    visitems.length = 0;
    visitems.assumeSafeAppend;
    foreach (immutable iidx, const ref it; items) {
      if (it.filterMatch(filter)) {
        visitems ~= VisItem(cast(int)iidx);
      }
    }
  }

  void removeItem (int idx) {
    if (idx < 0 || idx >= items.length) return;
    int oidx = itemIndex;
    foreach (immutable c; idx+1..items.length) items[c-1] = items[c];
    items.length -= 1;
    items.assumeSafeAppend;
    if (oidx >= items.length) oidx = cast(int)(items.length-1);
    if (oidx < 0) oidx = 0;
    refilter();
    itemIndex = oidx;
  }

  int lastFullVisible () {
    if (visitems.length == 0) return 0;
    int res = topItemIdx;
    int y = topY+Padding+menuTitle.h;
    int idx = topItemIdx;
    if (idx < 0) idx = 0;
    int ey = calcBotY-Padding;
    while (idx < visitems.length) {
      y += itAt(idx).h;
      if (y > ey) return res;
      res = idx;
      ++idx;
    }
    return cast(int)visitems.length-1;
  }

  int calcBotY () {
    int y = topY+Padding*2+menuTitle.h;
    foreach (const ref sc; items) {
      int ny = y+sc.h;
      if (ny > GHeight-Padding*2) return y;
      y = ny;
    }
    return y;
  }

  void normPage () {
    if (curItemIdx < 0) curItemIdx = 0;
    if (curItemIdx >= visitems.length) curItemIdx = cast(int)visitems.length-1;
    if (visitems.length) {
      // make current item visible
      if (curItemIdx >= 0 && curItemIdx < visitems.length) {
        if (curItemIdx < topItemIdx) {
          topItemIdx = curItemIdx;
        } else {
          //FIXME: make this faster!
          //conwriteln("top=", topItemIdx, "; cur=", curItemIdx, "; lv=", lastFullVisible);
          while (lastFullVisible < curItemIdx) {
            if (topItemIdx >= visitems.length-1) break;
            ++topItemIdx;
          }
        }
      }
    }
    while (topItemIdx > 0) {
      int lv = lastFullVisible;
      --topItemIdx;
      int lv1 = lastFullVisible;
      if (lv1 != lv) { ++topItemIdx; break; }
    }
  }

  void draw () {
    vg.save();
    scope(exit) vg.restore();
    if (moving) vg.globalAlpha(0.8);
    vg.fontFaceId(uiFont);
    vg.textAlign(NVGTextAlign.H.Left, NVGTextAlign.V.Baseline);
    vg.fontSize(fsizeUI);
    normPage();
    int ey = calcBotY;
    //writeln("leftX=", leftX, "; topY=", topY, "; w=", maxW+Padding*2, "; h=", ey-topY);
    vg.bndMenuBackground(leftX, topY, maxW+Padding*2, ey-topY, BND_CORNER_NONE);
    int y = topY+Padding;
    vg.scissor(leftX+Padding, y, maxW, ey-Padding);
    if (hasTitle && filter.length == 0) {
      vg.bndMenuLabel(leftX+Padding, y, maxW, menuTitle.h, -1, menuTitle.s);
      y += menuTitle.h;
    }
    if (filter.length != 0 && hasTitle) { //FIXME
      char[256] buf = void;
      char[4] ub = void;
      int bufpos = 0;
      foreach (char fch; filter) {
        auto len = utf8Encode(ub[], koi2uni(fch));
        if (len < 1) continue;
        if (bufpos+len > buf.length) break;
        buf[bufpos..bufpos+len] = ub[0..len];
        bufpos += len;
      }
      vg.bndMenuLabel(leftX+Padding, y, maxW, menuTitle.h, -1, buf[0..bufpos]);
      y += menuTitle.h;
    }
    int idx = topItemIdx;
    if (idx < 0) idx = 0;
    ey -= Padding;
    while (idx < visitems.length) {
      vg.bndMenuItem(leftX+Padding, y, maxW, itAt(idx).h,
        (curItemIdx == idx ? BND_ACTIVE : hoverItem == idx ? BND_HOVER : BND_DEFAULT),
        -1, itAt(idx).s);
      y += itAt(idx).h;
      if (y >= ey) break;
      ++idx;
    }
    needRedraw = false;
  }

  enum int Close = -666;
  enum int Eaten = -2;
  enum int NotMine = -1;

  int onKey (KeyEvent event) {
    if (!event.pressed) return NotMine;
    switch (event.key) {
      case Key.Escape:
        return Close;
      case Key.Up:
        if (curItemIdx > 0) {
          needRedraw = true;
          --curItemIdx;
        }
        return Eaten;
      case Key.Down:
        if (curItemIdx+1 < visitems.length) {
          needRedraw = true;
          ++curItemIdx;
        }
        return Eaten;
      case Key.PageUp:
        if (visitems.length) {
          if (curItemIdx == topItemIdx) {
            while (topItemIdx > 0 && lastFullVisible != curItemIdx) --topItemIdx;
          }
          needRedraw = true;
          curItemIdx = topItemIdx;
        }
        return Eaten;
      case Key.PageDown:
        if (visitems.length) {
          if (curItemIdx == lastFullVisible) {
            while (lastFullVisible < visitems.length && topItemIdx != curItemIdx) ++topItemIdx;
          }
          needRedraw = true;
          curItemIdx = lastFullVisible;
        }
        return Eaten;
      case Key.Home:
        curItemIdx = 0;
        needRedraw = true;
        return Eaten;
      case Key.End:
        if (visitems.length) {
          curItemIdx = cast(int)visitems.length-1;
          needRedraw = true;
        }
        return Eaten;
      case Key.Enter:
        return (isCurValid ? itemIndex : Close);
      default:
    }
    // clear filter
    if (allowFiltering && event == "C-Y") {
      if (filter.length) {
        filter.length = 0;
        filter.assumeSafeAppend;
        auto cidx = itemIndex;
        refilter();
        itemIndex = cidx;
        needRedraw = true;
        return Eaten;
      }
    }
    if (allowFiltering && filter.length && event == "Backspace") {
      filter.length -= 1;
      filter.assumeSafeAppend;
      auto cidx = itemIndex;
      refilter();
      itemIndex = cidx;
      needRedraw = true;
      return Eaten;
    }
    return NotMine;
  }

  int onChar (dchar dch) {
    if (!allowFiltering || dch < ' ') return NotMine;
    char ch = uni2koi(dch);
    if (ch < ' ' || ch == 127) return NotMine;
    filter ~= ch;
    auto cidx = itemIndex;
    refilter();
    itemIndex = cidx;
    needRedraw = true;
    return Eaten;
  }

  int onMouse (MouseEvent event) {
    int atItem = -1;
    bool inside = false;
    if (event.x >= leftX+Padding && event.x < leftX+Padding+maxW && event.y >= topY+Padding) {
      normPage();
      int ey = calcBotY-Padding;
      if (event.y < ey) {
        inside = true;
        int y = topY+Padding;
        if (hasTitle) y += menuTitle.h;
        int idx = topItemIdx;
        if (idx < 0) idx = 0;
        while (idx < visitems.length) {
          if (event.y >= y && event.y < y+itAt(idx).h) { atItem = idx; break; }
          y += itAt(idx).h;
          if (y >= ey) break;
          ++idx;
        }
      }
    }
    if (hoverItem != atItem) {
      hoverItem = atItem;
      needRedraw = true;
    }
    switch (event.type) {
      case MouseEventType.motion:
        if (moving) {
          leftX += event.dx;
          topY += event.dy;
          needRedraw = true;
        }
        break;
      case MouseEventType.buttonPressed:
        if (event.button == MouseButton.right) return Close;
        if (event.button == MouseButton.left) {
          //if (hoverItem >= 0 && hoverItem < visitems.length) return hoverItem;
          if (hoverItem >= 0 && hoverItem < visitems.length) {
            curItemIdx = hoverItem;
          } else {
            if (inside) {
              moving = true;
              needRedraw = true;
            }
          }
        }
        if (event.button == MouseButton.wheelUp) {
          //if (topItemIdx > 0) { --topItemIdx; needRedraw = true; }
          if (curItemIdx > 0) { --curItemIdx; needRedraw = true; }
        }
        if (event.button == MouseButton.wheelDown) {
          //if (topItemIdx < visitems.length) { ++topItemIdx; needRedraw = true; }
          if (curItemIdx < visitems.length) { ++curItemIdx; needRedraw = true; }
        }
        break;
      case MouseEventType.buttonReleased:
        if (event.button == MouseButton.left) {
          //if (curItemIdx >= 0 && curItemIdx < visitems.length) return curItemIdx;
          if (curItemIdx == hoverItem && curItemIdx >= 0 && curItemIdx < visitems.length && isCurValid) return itemIndex;
          moving = false;
          needRedraw = true;
        }
        break;
      default:
    }
    return NotMine;
  }
}
