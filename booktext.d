/* Written by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module booktext /*is aliced*/;

import std.encoding;

import arsd.image;

import iv.base64;
import iv.cmdcon;
import iv.encoding;
//import iv.iresample;
import iv.nanovega;
import iv.nanovega.textlayouter;
import iv.saxy;
import iv.strex;
import iv.utfutil;
import iv.vfs;


// ////////////////////////////////////////////////////////////////////////// //
struct BookInfo {
  string author;
  string title;
  string seqname;
  uint seqnum;
  string diskfile; // not set by `loadBookInfo()`!
  // for db
  int id;
  bool needupdate;
}


BookInfo loadBookInfo (const(char)[] fname) {
  static BookInfo loadFile (VFile fl) {
    BookInfo res;
    auto sax = new SaxyEx();
    bool complete = false;
    string authorFirst, authorLast;

    inout(char)[] strip (inout(char)[] s) inout {
      while (s.length && s.ptr[0] <= ' ') s = s[1..$];
      while (s.length && s[$-1] <= ' ') s = s[0..$-1];
      return s;
    }

    try {
      // sequence tag
      sax.onOpen("/FictionBook/description/title-info/sequence", (char[] text, char[][string] attrs) {
        if (auto sn = "name" in attrs) {
          res.seqname = strip(*sn).dup;
          if (auto id = "number" in attrs) {
            import std.conv : to;
            res.seqnum = to!uint(*id);
            if (res.seqnum < 1 || res.seqnum > 8192) res.seqname = null;
          }
        } else {
          res.seqname = null;
        }
        if (res.seqname.length == 0) { res.seqname = null; res.seqnum = 0; }
      });

      sax.onClose("/FictionBook/description", (char[] text) {
        complete = true;
        throw new Exception("done"); // sorry
      });

      // author's first name
      sax.onContent("/FictionBook/description/title-info/author/first-name", (char[] text) {
        authorFirst = strip(text).idup;
      });
      // author's last name
      sax.onContent("/FictionBook/description/title-info/author/last-name", (char[] text) {
        authorLast = strip(text).idup;
      });
      // book title
      sax.onContent("/FictionBook/description/title-info/book-title", (char[] text) {
        res.title = strip(text).idup;
      });

      sax.loadStream(fl);
    } catch (Exception e) {
      if (!complete) throw e;
    }
    if (authorFirst.length == 0) {
      authorFirst = authorLast;
      authorLast = null;
    } else if (authorLast.length != 0) {
      authorFirst ~= " "~authorLast;
    }
    if (authorFirst.length == 0 && res.title.length == 0) throw new Exception("no book title found");
    res.author = authorFirst;
    return res;
  }

  import std.path : extension;
  if (strEquCI(fname.extension, ".zip")) {
    auto did = vfsAddPak!"first"(fname);
    scope(exit) vfsRemovePak(did);
    foreach (immutable idx, ref de; vfsFileList) {
      if (strEquCI(de.name.extension, ".fb2")) return loadFile(vfsOpenFile(de.name));
    }
    throw new Exception("no fb2 file found in '"~fname.idup~"'");
  } else {
    return loadFile(vfsOpenFile(fname));
  }
}


// ////////////////////////////////////////////////////////////////////////// //
final class Tag {
  string name; // empty: text node
  string id;
  string href;
  string text;
  Tag[] children;
  Tag parent;
  //Tag prevSibling;
  //Tag nextSibling;

  @property inout(Tag) firstChild () inout pure nothrow @trusted @nogc { pragma(inline, true); return (children.length ? children.ptr[0] : null); }
  @property inout(Tag) lastChild () inout pure nothrow @trusted @nogc { pragma(inline, true); return (children.length ? children[$-1] : null); }

  string textContent () const {
    if (name.length == 0) return text;
    string res;
    foreach (const Tag t; children) res ~= t.textContent;
    return res;
  }

  string toStringNice (int indent=0) const {
    string res;
    void addIndent () { foreach (immutable _; 0..indent) res ~= ' '; }
    void newLine () { res ~= '\n'; foreach (immutable _; 0..indent) res ~= ' '; }
    if (name.length == 0) return text;
    res ~= '<';
    if (children.length == 0) res ~= '/';
    res ~= name;
    if (id.length) { res ~= " id="; res ~= id; }
    if (href.length) { res ~= " href="; res ~= href; }
    res ~= '>';
    if (children.length) {
      if (indent >= 0) indent += 2;
      foreach (const Tag cc; children) {
        if (indent >= 0) newLine();
        res ~= cc.toStringNice(indent);
      }
      if (indent >= 0) indent -= 2;
      if (indent >= 0) newLine();
      res ~= "</";
      res ~= name;
      res ~= ">";
    }
    return res;
  }

  override string toString () const { return toStringNice(int.min); }
}


// ////////////////////////////////////////////////////////////////////////// //
final class BookText {
  string authorFirst;
  string authorLast;
  string title;
  string sequence;
  uint seqnum;

  static struct Image {
    string id;
    TrueColorImage img;
    int nvgid = -1;
  }

  Tag content; // body
  Image[] images;

  this (const(char)[] fname) { loadFile(fname); }
  this (VFile fl) { loadFile(fl); }

  string getAuthor () {
    if (authorLast.length != 0) return authorFirst~" "~authorLast;
    return authorFirst;
  }

  string getTitle () => title;

private:
  void loadFile (VFile fl) {
    auto sax = new SaxyEx();

    string norm (const(char)[] s) {
      string res;
      foreach (char ch; s) {
        if (ch <= ' ' || ch == 127) {
          if (res.length && res[$-1] > ' ') res ~= ch;
        } else {
          res ~= ch;
        }
      }
      return res;
    }

    // sequence tag
    sax.onOpen("/FictionBook/description/title-info/sequence", (char[] text, char[][string] attrs) {
      if (auto sn = "name" in attrs) {
        sequence = (*sn).dup;
        if (auto id = "number" in attrs) {
          import std.conv : to;
          seqnum = to!uint(*id);
          if (seqnum < 1 || seqnum > 8192) sequence = null;
        }
      } else {
        sequence = null;
      }
      if (sequence.length == 0) { sequence = null; seqnum = 0; }
    });

    // author's first name
    sax.onContent("/FictionBook/description/title-info/author/first-name", (char[] text) {
      authorFirst = norm(text);
    });
    // author's last name
    sax.onContent("/FictionBook/description/title-info/author/last-name", (char[] text) {
      authorLast = norm(text);
    });
    // book title
    sax.onContent("/FictionBook/description/title-info/book-title", (char[] text) {
      title = norm(text);
    });

    if (authorFirst.length == 0) {
      authorFirst = authorLast;
      authorLast = null;
    }

    string imageid;
    string imagefmt;
    char[] imagectx;

    sax.onOpen("/FictionBook/binary", (char[] text, char[][string] attrs) {
      import iv.vfs.io;
      if (auto ctp = "content-type" in attrs) {
        if (*ctp == "image/png" || *ctp == "image/jpeg" || *ctp == "image/jpg") {
          if (auto idp = "id" in attrs) {
            imageid = (*idp).idup;
            if (imageid.length == 0) {
              conwriteln("image without id");
            } else {
              imagefmt = (*ctp).idup;
            }
          }
        } else {
          conwriteln("unknown binary content format: '", *ctp, "'");
        }
      }
    });
    sax.onContent("/FictionBook/binary", (char[] text) {
      if (imageid.length) {
        foreach (char ch; text) if (ch > ' ') imagectx ~= ch;
      }
    });
    sax.onClose("/FictionBook/binary", (char[] text) {
      import iv.vfs.io;
      if (imageid.length) {
        //conwriteln("image with id '", imageid, "'...");
        //import std.base64;
        if (imagectx.length == 0) {
          conwriteln("image with id '", imageid, "' has no data");
        } else {
          try {
            //auto imgdata = Base64.decode(imagectx);
            auto imgdata = base64Decode(imagectx);
            TrueColorImage img;
            MemoryImage memimg;
            if (imagefmt == "image/jpeg" || imagefmt == "image/jpg") {
              memimg = readJpegFromMemory(imgdata[]);
            } else if (imagefmt == "image/png") {
              memimg = imageFromPng(readPng(imgdata[]));
            } else {
              assert(0, "wtf?!");
            }
            if (memimg is null) {
              conwriteln("fucked image, id '", imageid, "'");
            } else {
              img = cast(TrueColorImage)memimg;
              if (img is null) {
                img = memimg.getAsTrueColorImage;
                delete memimg;
              }
              if (img.width > 1 && img.height > 1) {
                if (img.width > 1024 || img.height > 1024) {
                  // scale image
                  float scl = 1024.0f/(img.width > img.height ? img.width : img.height);
                  int nw = cast(int)(img.width*scl);
                  int nh = cast(int)(img.height*scl);
                  if (nw < 1) nw = 1;
                  if (nh < 1) nh = 1;
                  img = imageResize!3(img, nw, nh);
                }
              }
              images ~= Image(imageid, img);
              //conwritePng("z_"~imageid~".png", images[$-1].img);
            }
          } catch (Exception e) {
            conwriteln("image with id '", imageid, "' has invalid data");
            conwriteln("ERROR: ", e.msg);
          }
        }
      }
      imageid = null;
      imagectx = null;
      imagefmt = null;
    });

    Tag[] tagStack;

    sax.onOpen("/FictionBook/body", (char[] text) {
      if (content is null) {
        content = new Tag();
        content.name = "body";
      }
      tagStack ~= content;
    });

    sax.onClose("/FictionBook/body", (char[] text) {
      if (content is null) assert(0, "wtf?!");
      tagStack.length -= 1;
      tagStack.assumeSafeAppend;
    });

    sax.onOpen("/FictionBook/body/+", (char[] text, char[][string] attrs) {
      auto tag = new Tag();
      tag.name = text.idup;
      tag.parent = tagStack[$-1];
      /*
      if (auto ls = tag.parent.lastChild) {
        ls.nextSibling = tag;
        tag.prevSibling = ls;
      }
      */
      tag.parent.children ~= tag;
      if (auto p = "id" in attrs) tag.id = norm(*p);
           if (auto p = "href" in attrs) tag.href = norm(*p);
      else if (auto p = "l:href" in attrs) tag.href = norm(*p);
      /*
      if (tag.name == "image") {
        import iv.vfs.io;
        conwriteln("IMAGE: ", tag.href);
      }
      */
      tagStack ~= tag;
    });

    sax.onClose("/FictionBook/body/+", (char[] text) {
      tagStack.length -= 1;
      tagStack.assumeSafeAppend;
    });

    sax.onContent("/FictionBook/body/+", (char[] text) {
      auto tag = new Tag();
      tag.name = null;
      tag.parent = tagStack[$-1];
      /*
      if (auto ls = tag.parent.lastChild) {
        ls.nextSibling = tag;
        tag.prevSibling = ls;
      }
      */
      tag.parent.children ~= tag;
      tag.text ~= text.idup;
    });

    sax.loadStream(fl);

    if (content is null) {
      content = new Tag();
      content.name = "body";
    }
  }

  void loadFile (const(char)[] fname) {
    import std.path : extension;
    if (strEquCI(fname.extension, ".zip")) {
      auto did = vfsAddPak!"first"(fname);
      scope(exit) vfsRemovePak(did);
      foreach (immutable idx, ref de; vfsFileList) {
        if (strEquCI(de.name.extension, ".fb2")) { loadFile(vfsOpenFile(de.name)); return; }
      }
      throw new Exception("no fb2 file found in '"~fname.idup~"'");
    } else {
      loadFile(vfsOpenFile(fname));
    }
  }
}
