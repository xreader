/* Written by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module xiniz /*is aliced*/;

import iv.nanovega;
import iv.strex;
import iv.vfs;
import iv.vfs.io;


// ////////////////////////////////////////////////////////////////////////// //
void xiniParse(A...) (VFile fl, A args) {
  import std.traits;
  char[] line, linebuf;
  scope(exit) line.destroy;
  bool eof = false;
  while (!eof) {
    line = null;
    auto sz = line.length;
    for (;;) {
      char ch = void;
      if (fl.rawRead((&ch)[0..1]).length == 0) { eof = true; break; }
      if (ch == '\n') break;
      if (sz >= linebuf.length) {
        if (sz > 1024*1024) throw new Exception("line too long");
        line = null;
        linebuf.assumeSafeAppend;
        linebuf.length = sz+1024;
      }
      linebuf.ptr[sz++] = ch;
    }
    if (sz == 0) continue;
    line = linebuf[0..sz];
    while (line.length && line[0] <= ' ') line = line[1..$];
    while (line.length && line[$-1] <= ' ') line = line[0..$-1];
    if (line.length == 0 || line[0] == '#' || line[0] == ';') continue;
    uint eqp = 0;
    while (eqp < line.length && line.ptr[eqp] != '=') ++eqp;
    if (eqp >= line.length) continue;
    auto name = line[0..eqp];
    auto value = line[eqp+1..$];
    while (name.length && name[0] <= ' ') name = name[1..$];
    while (name.length && name[$-1] <= ' ') name = name[0..$-1];
    while (value.length && value[0] <= ' ') value = value[1..$];
    while (value.length && value[$-1] <= ' ') value = value[0..$-1];
    //{ import std.stdio; writeln(name.quote, "=", value.quote); }
    bool found = false;
    foreach (immutable idx, immutable TA; A) {
      //pragma(msg, idx, " : ", A[idx]);
      static if (idx%2 == 0) {
        static if (is(TA == string)) {
          if (args[idx] == name) {
            found = true;
            static if (isCallable!(A[idx+1])) {
              args[idx+1](name, value);
            } else static if (is(A[idx+1] : T*, T)) {
              import std.conv : to;
              static if (is(T == ubyte) || is(T == ushort) || is(T == uint) || is(T == ulong)) {
                if (value.length > 2 && value[0] == '0' && (value[1] == 'x' || value[1] == 'X')) {
                  *(args[idx+1]) = to!T(value[2..$], 16);
                } else {
                  *(args[idx+1]) = to!T(value);
                }
              } else static if (is(T == NVGColor)) {
                uint v;
                if (value.length > 2 && value[0] == '0' && (value[1] == 'x' || value[1] == 'X')) {
                  v = to!uint(value[2..$], 16);
                  if (value.length <= 2+3*2) v |= 0xff000000;
                } else {
                  v = to!uint(value);
                  v |= 0xff000000;
                }
                *(args[idx+1]) = NVGColor.fromUintARGB(v);
                //writefln("v=0x%08x; r=%s; g=%s; b=%s; a=%s", v, (*(args[idx+1])).r, (*(args[idx+1])).g, (*(args[idx+1])).b, (*(args[idx+1])).a);
              } else {
                *(args[idx+1]) = to!T(value);
              }
              //{ import std.stdio; writeln("TA: ", args[idx].quote); }
            } else {
              static assert(0, "argument for value should be delegate or pointer");
            }
          } else if (args[idx].length == name.length+1 && args[idx][0] == '?' && args[idx][1..$] == name) {
            // this should be bool -- "i've seen her" flag
            static if (isCallable!(A[idx+1])) {
            } else static if (is(A[idx+1] : T*, T)) {
              import std.conv : to;
              //pragma(msg, "b: ", T);
              *(args[idx+1]) = to!T(true);
            } else {
              static assert(0, "argument for checked should be var");
            }
          }
        }
      }
    }
    // call "unknown key" callback
    static if (A.length%2 && isCallable!(A[$-1])) {
      if (!found) args[$-1](name, value);
    }
  }
}


/*
void main () {
  int sec = -1, par = -1, word = -1;
  bool wasSec = false;
  xiniParse(VFile("z00.rc"),
    "section", &sec,
    "?section", &wasSec,
    "paragraph", &par,
    "word", &word,
    "boo", (const(char)[] name, const(char)[] value) { import std.stdio; writeln("<", name, ">=<", value, ">"); },
  );
  import std.stdio;
  writeln("sec=", sec);
  writeln("par=", par);
  writeln("word=", word);
  writeln("wasSec=", wasSec);
}
*/
