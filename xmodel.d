/* Written by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module xmodel;

import iv.cmdcon;
import iv.glbinds;
import iv.vfs;
import iv.vmath;


// ////////////////////////////////////////////////////////////////////////// //
final class EliteModel {
public:
  static align(1) struct UV {
  align(1):
    float u, v;
  }

  static struct Texture {
    uint width, height;
    ubyte* data;
    uint tid;
  }

static:
  T* xalloc(T) (uint count) {
    import core.stdc.stdlib : malloc;
    import core.stdc.string : memset;
    assert(count > 0);
    auto res = malloc(T.sizeof*count);
    if (res is null) assert(0, "out of memory");
    memset(res, 0, T.sizeof*count);
    return cast(T*)res;
  }

  void xfree(T) (ref T* ptr) {
    if (ptr !is null) {
      import core.stdc.stdlib : free;
      free(ptr);
      ptr = null;
    }
  }

public:
  vec3[2] bbox;

  uint texCount;
  Texture* texts;

  uint faceCount;
  uint* mids; // face material array
  vec3* verts;
  vec3* norms;
  UV* uvs;
  uint* inds; // just a big array of indexes
  uint subCount; // submodels
  uint* subfcs; // faces in each submodel
  ubyte* subsms; // smoothness

  // exhaust
  uint exCount;
  vec3* exPos;
  vec3* exSize;

  string name;
  string idName;
  string dispName;

public:
  this (const(char)[] fname) { load(fname); }
  this (VFile fl) { load(fl); }

  // don't unload textures here, as OpenGL context may be already destroyed
  ~this () { freeData(); }

  void freeData () {
    if (texCount > 0) {
      foreach (immutable idx; 0..texCount) xfree(texts[idx].data);
      xfree(texts);
      texCount = 0;
    }
    xfree(mids);
    xfree(verts);
    xfree(norms);
    xfree(uvs);
    xfree(inds);
    xfree(subfcs);
    xfree(subsms);
    xfree(exPos);
    xfree(exSize);
    subCount = 0;
    exCount = 0;
    name = null;
    idName = null;
    dispName = null;
  }

  void freeImages () {
    import core.stdc.stdlib : free;
    foreach (immutable idx; 0..texCount) xfree(texts[idx].data);
  }

  void glUnload () {
    glBindTexture(GL_TEXTURE_2D, 0);
    foreach (immutable idx; 0..texCount) {
      if (texts[idx].tid) {
        glDeleteTextures(1, &texts[idx].tid);
        texts[idx].tid = 0;
      }
    }
  }

  void glUpload () {
    foreach (immutable idx; 0..texCount) {
      if (texts[idx].tid) continue;
      if (texts[idx].data is null) continue;

      uint wrapOpt = GL_REPEAT;
      uint filterOpt = GL_LINEAR;

      glGenTextures(1, &texts[idx].tid);
      glBindTexture(GL_TEXTURE_2D, texts[idx].tid);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapOpt);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapOpt);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filterOpt);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filterOpt);
      //float[4] bclr = 0.0;
      //glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, bclr.ptr);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texts[idx].width, texts[idx].height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texts[idx].data);
    }
  }

  static uint glClearFlags () { pragma(inline, true); return (GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT); }

  // setup model and view matrices before calling this
  // also, setup lighting
  void glDraw () {
    if (texCount == 0) return;

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
    //glDisable(GL_LIGHTING);
    glDisable(GL_DITHER);
    glDisable(GL_BLEND);

    glEnable(GL_RESCALE_NORMAL);
    glDisable(GL_SCISSOR_TEST);
    glDisable(GL_STENCIL_TEST);
    glDisable(GL_COLOR_MATERIAL);

    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glEnable(GL_CULL_FACE);
    //glCullFace(GL_FRONT);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);

    glDisable(GL_CULL_FACE); // this way we can draw any model

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glVertexPointer(3, GL_FLOAT, 0, verts);
    glNormalPointer(GL_FLOAT, 0, norms);
    glTexCoordPointer(2, GL_FLOAT, 0, uvs);

    uint s = 0;
    foreach (immutable smidx, uint len; subfcs[0..subCount]) {
      auto end = s+len;
      glShadeModel(subsms[smidx] ? GL_SMOOTH : GL_FLAT);
      while (s < faceCount && s < end) {
        uint tid = mids[s];
        uint e = s;
        while (e < faceCount && e < end && mids[e] == tid) ++e;
        glBindTexture(GL_TEXTURE_2D, texts[tid].tid);
        glDrawElements(GL_TRIANGLES, (e-s)*3, GL_UNSIGNED_INT, inds+s*3);
        s = e;
      }
    }

    glBindTexture(GL_TEXTURE_2D, 0);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  }

  void glDrawExhaust () {
    // "12.87 -6.34 -54.9 7.45 7.46 6"
    // "-12.87 -6.34 -54.9 7.45 7.46 6"

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
    //glDisable(GL_LIGHTING);
    glDisable(GL_DITHER);
    glEnable(GL_COLOR_MATERIAL);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glEnable(GL_CULL_FACE);
    //glCullFace(GL_FRONT);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);

    glDisable(GL_CULL_FACE); // this way we can draw any model
    glShadeModel(GL_FLAT);

    void drawTri() (in auto ref vec3 p0, in auto ref vec3 p1, in auto ref vec3 p2) {
      // calculate normal
      vec3 n = (p1-p0).cross(p2-p0).normalized;
      glBegin(GL_TRIANGLES);
        glNormal3f(n.x, n.y, n.z);
        glVertex3f(p0.x, p0.y, p0.z);
        glVertex3f(p1.x, p1.y, p1.z);
        glVertex3f(p2.x, p2.y, p2.z);
      glEnd();
    }

    void drawCone() (/*in auto ref*/ vec3 pos, /*in auto ref*/ vec3 size) {
      //float zlen = size.z;
      //if (zlen < 0) zlen = -zlen;
      auto zlen = bbox[1].z-bbox[0].z;
      if (zlen < 0) zlen = -zlen;
      zlen /= 3.0;
      //zlen *= 2.0;
      glColor4f(0, 0.5, 0.8, 0.2);
      //pos *= 0.5;
      size *= 0.5;
      drawTri(
        pos-vec3(size.x, size.y, 0),
        pos-vec3(0, 0, -zlen),
        pos-vec3(size.x, -size.y, 0),
      );
      drawTri(
        pos-vec3(-size.x, -size.y, 0),
        pos-vec3(0, 0, -zlen),
        pos-vec3(-size.x, size.y, 0),
      );
      drawTri(
        pos-vec3(-size.x, size.y, 0),
        pos-vec3(0, 0, -zlen),
        pos-vec3(size.x, size.y, 0),
      );
      drawTri(
        pos-vec3(size.x, -size.y, 0),
        pos-vec3(0, 0, -zlen),
        pos-vec3(-size.x, -size.y, 0),
      );
    }

    //drawCone(vec3(12.87, -6.34, 54.9), vec3(7.45, 7.46, 6));
    foreach (immutable ei; 0..exCount) {
      drawCone(exPos[ei], exSize[ei]);
    }

    glColor4f(1, 1, 1, 1);
    glDisable(GL_CULL_FACE); // this way we can draw any model
    glDisable(GL_BLEND);
  }

private:
  void load (const(char)[] fname) {
    import std.path;
    load(VFile(fname));
    if (name.length == 0) {
      name = fname.baseName.stripExtension.idup;
      if (name.length > 3 && name[0..4] == "oxm:") name = name[4..$];
    }
    if (idName.length == 0) idName = name;
    if (dispName.length == 0) dispName = name;
  }

  void load (VFile fl) {
    scope(failure) freeData();
    // signature
    char[4] sign;
    fl.rawReadExact(sign[]);

    if (sign == "EMD1") {
      // old model format
      // number of faces
      auto fcount = fl.readNum!ushort;
      if (fcount == 0) throw new Exception("invalid model file");
      // number of textures
      auto tcount = fl.readNum!ushort;
      if (tcount == 0 || tcount > fcount) throw new Exception("invalid model file");
      faceCount = fcount;
      mids = xalloc!uint(fcount);
      verts = xalloc!vec3(fcount*3);
      norms = xalloc!vec3(fcount*3);
      uvs = xalloc!UV(fcount*3);
      inds = xalloc!uint(fcount*3);
      subCount = 1;
      subfcs = xalloc!uint(fcount);
      subfcs[0] = fcount;
      subsms = xalloc!ubyte(fcount);
      subsms[0] = 1;
      foreach (immutable idx, ref n; inds[0..fcount*3]) n = cast(uint)idx; // yep
      exCount = 0;
      // faces
      foreach (immutable fidx; 0..fcount) {
        // texture id
        mids[fidx] = fl.readNum!ushort;
        if (mids[fidx] >= tcount) throw new Exception("invalid model file");
        // three vertices
        foreach (immutable vn; 0..3) {
          // uv
          uvs[fidx*3+vn].u = fl.readNum!float;
          uvs[fidx*3+vn].v = fl.readNum!float;
          // coords
          verts[fidx*3+vn].x = fl.readNum!float;
          verts[fidx*3+vn].y = fl.readNum!float;
          verts[fidx*3+vn].z = fl.readNum!float;
          // normal
          norms[fidx*3+vn].x = fl.readNum!float;
          norms[fidx*3+vn].y = fl.readNum!float;
          norms[fidx*3+vn].z = fl.readNum!float;
        }
      }
      texCount = tcount;
      texts = xalloc!Texture(tcount);
      // textures
      foreach (immutable idx; 0..tcount) {
        // dimensions
        auto w = fl.readNum!ushort;
        auto h = fl.readNum!ushort;
        if (w < 1 || h < 1) throw new Exception("invalid model file");
        // compressed size
        auto csz = fl.readNum!uint;
        if (csz == 0 || csz > int.max) throw new Exception("invalid model file");
        auto ep = fl.tell;
        {
          auto zs = wrapZLibStreamRO(fl, VFSZLibMode.ZLib, w*h*4, ep, csz);
          //auto data = new ubyte[](w*h*4);
          ubyte* data = xalloc!ubyte(w*h*4);
          scope(failure) xfree(data);
          zs.rawReadExact(data[0..w*h*4]);
          //texts ~= Texture(w, h, data, 0);
          texts[idx].width = w;
          texts[idx].height = h;
          texts[idx].data = data;
          texts[idx].tid = 0;
          data = null;
        }
        fl.seek(ep+csz);
      }
    } else if (sign == "EMD2") {
      // new model format
      // number of faces
      auto fcount = fl.readNum!ushort;
      if (fcount == 0) throw new Exception("invalid model file");
      // number of textures
      auto tcount = fl.readNum!ushort;
      if (tcount == 0 || tcount > fcount) throw new Exception("invalid model file");
      // number of submodels
      auto scount = fl.readNum!ushort;
      if (scount == 0 || scount > fcount) throw new Exception("invalid model file");
      // number of exhausts
      auto ecount = fl.readNum!ushort;
      if (/*ecount == 0 ||*/ ecount > 255) throw new Exception("invalid model file");
      faceCount = fcount;
      mids = xalloc!uint(fcount);
      verts = xalloc!vec3(fcount*3);
      norms = xalloc!vec3(fcount*3);
      uvs = xalloc!UV(fcount*3);
      inds = xalloc!uint(fcount*3);
      subCount = scount;
      subfcs = xalloc!uint(scount);
      subsms = xalloc!ubyte(scount);
      foreach (immutable idx, ref n; inds[0..fcount*3]) n = cast(uint)idx; // yep
      exCount = ecount;
      if (ecount) {
        exPos = xalloc!vec3(ecount);
        exSize = xalloc!vec3(ecount);
      }

      string readStr () {
        auto len = fl.readNum!ubyte;
        if (len) {
          auto s = new char[](len);
          fl.rawReadExact(s[]);
          return cast(string)s; // it is safe to cast here
        } else {
          return null;
        }
      }
      name = readStr;
      idName = readStr;
      dispName = readStr;

      // face counts
      foreach (ref uint c; subfcs[0..scount]) c = fl.readNum!ushort;
      // face smoothness
      //foreach (ref ubyte v; subsms[0..scount]) v = fl.readNum!ubyte;
      fl.rawReadExact(subsms[0..scount]);

      // exhausts
      foreach (immutable ei; 0..ecount) {
        exPos[ei].x = fl.readNum!float;
        exPos[ei].y = fl.readNum!float;
        exPos[ei].z = fl.readNum!float;
        exSize[ei].x = fl.readNum!float;
        exSize[ei].y = fl.readNum!float;
        exSize[ei].z = fl.readNum!float;
      }

      // faces
      foreach (immutable fidx; 0..fcount) {
        // texture id
        mids[fidx] = fl.readNum!ushort;
        if (mids[fidx] >= tcount) throw new Exception("invalid model file");
        // three vertices
        foreach (immutable vn; 0..3) {
          // uv
          uvs[fidx*3+vn].u = fl.readNum!float;
          uvs[fidx*3+vn].v = fl.readNum!float;
          // coords
          verts[fidx*3+vn].x = fl.readNum!float;
          verts[fidx*3+vn].y = fl.readNum!float;
          verts[fidx*3+vn].z = fl.readNum!float;
          // normal
          norms[fidx*3+vn].x = fl.readNum!float;
          norms[fidx*3+vn].y = fl.readNum!float;
          norms[fidx*3+vn].z = fl.readNum!float;
        }
      }

      // material (texture) colors
      /*
      foreach (immutable _; 0..tcount) {
        fl.writeNum!float(mat.colorAmb[0]);
        fl.writeNum!float(mat.colorAmb[1]);
        fl.writeNum!float(mat.colorAmb[2]);
        fl.writeNum!float(mat.colorAmb[3]);
        fl.writeNum!float(mat.colorDiff[0]);
        fl.writeNum!float(mat.colorDiff[1]);
        fl.writeNum!float(mat.colorDiff[2]);
        fl.writeNum!float(mat.colorDiff[3]);
        fl.writeNum!float(mat.colorSpec[0]);
        fl.writeNum!float(mat.colorSpec[1]);
        fl.writeNum!float(mat.colorSpec[2]);
        fl.writeNum!float(mat.colorSpec[3]);
        fl.writeNum!float(mat.shininess);
      }
      */
      fl.seek(tcount*(13*float.sizeof), Seek.Cur);

      // textures
      texCount = tcount;
      texts = xalloc!Texture(tcount);
      foreach (immutable idx; 0..tcount) {
        // dimensions
        auto w = fl.readNum!ushort;
        auto h = fl.readNum!ushort;
        if (w < 1 || h < 1) throw new Exception("invalid model file");
        auto compType = fl.readNum!ubyte;
        if (compType == 0) {
          ubyte* data = xalloc!ubyte(w*h*4);
          scope(failure) xfree(data);
          fl.rawReadExact(data[0..w*h*4]);
          texts[idx].width = w;
          texts[idx].height = h;
          texts[idx].data = data;
          texts[idx].tid = 0;
          data = null;
        } else if (compType == 1) {
          // compressed size
          auto csz = fl.readNum!uint;
          if (csz == 0 || csz > int.max) throw new Exception("invalid model file");
          auto ep = fl.tell;
          {
            auto zs = wrapZLibStreamRO(fl, VFSZLibMode.ZLib, w*h*4, ep, csz);
            //auto data = new ubyte[](w*h*4);
            ubyte* data = xalloc!ubyte(w*h*4);
            scope(failure) xfree(data);
            zs.rawReadExact(data[0..w*h*4]);
            texts[idx].width = w;
            texts[idx].height = h;
            texts[idx].data = data;
            texts[idx].tid = 0;
            data = null;
          }
          fl.seek(ep+csz);
        } else {
          throw new Exception("invalid compression type");
        }
      }
    } else {
      throw new Exception("invalid model signature");
    }
    // calc bbox
    bbox[0] = vec3(float.max, float.max, float.max);
    bbox[1] = vec3(-float.max, -float.max, -float.max);
    foreach (const ref vec3 v; verts[0..faceCount*3]) {
      import std.algorithm : min, max;
      bbox[0].x = min(bbox[0].x, v.x);
      bbox[0].y = min(bbox[0].y, v.y);
      bbox[0].z = min(bbox[0].z, v.z);
      bbox[1].x = max(bbox[1].x, v.x);
      bbox[1].y = max(bbox[1].y, v.y);
      bbox[1].z = max(bbox[1].z, v.z);
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared bool doLights = true;
__gshared bool drawLights = false;
private {
  __gshared int lightCount = 0;
  __gshared float[4][8] lightColor;
  __gshared float[3][8] lightPos;
}


// ////////////////////////////////////////////////////////////////////////// //
void lightsClear () {
  lightCount = 0;
}


void lightAdd (float x, float y, float z, float r, float g, float b) {
  if (lightCount < 8) {
    lightColor[lightCount][0] = r;
    lightColor[lightCount][1] = g;
    lightColor[lightCount][2] = b;
    lightColor[lightCount][3] = 1.0f;
    lightPos[lightCount][0] = x;
    lightPos[lightCount][1] = y;
    lightPos[lightCount][2] = z;
    ++lightCount;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void drawModel (float angle, EliteModel shipModel) {
  import iv.glbinds;

  if (shipModel is null) return;

  //glClearColor(0.2, 0.2, 0.2, 0);
  //glClear(shipModel.glClearFlags);

  //glEnable(GL_DEPTH_TEST);
  //glDepthFunc(GL_LEQUAL);
  //glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  glClearDepth(1.0f);

  // setup projection
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  oglPerspective(
    90.0, // field of view in degree
    1.0, // aspect ratio
    1.0, // Z near
    10000.0, // Z far
  );
  float zz = shipModel.bbox[1].z-shipModel.bbox[0].z;
  zz += 10;
  //if (zz < 20) zz = 20;
  oglLookAt(
    0.0, 0.0, -zz, // eye is at (0,0,5)
    0.0, 0.0, 0, // center is at (0,0,0)
    0.0, 1.0, 0.0, // up is in positive Y direction
  );

  // setup model matrix
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  // setup lighting
  /*
  {
    static immutable float[4] ldiff0 = [1.0, 1.0, 0.0, 1.0];
    static immutable float[3] lpos0 = [-20.0, 0.0, -20.0-20];
    glLightfv(GL_LIGHT0, GL_DIFFUSE, ldiff0.ptr);
    glLightfv(GL_LIGHT0, GL_POSITION, lpos0.ptr);
    glEnable(GL_LIGHT0);
  }
  {
    static immutable float[4] ldiff1 = [1.0, 0.0, 0.0, 1.0];
    static immutable float[3] lpos1 = [20.0, -20.0, 20.0-20];
    glLightfv(GL_LIGHT1, GL_DIFFUSE, ldiff1.ptr);
    glLightfv(GL_LIGHT1, GL_POSITION, lpos1.ptr);
    glEnable(GL_LIGHT1);
  }
  */

  /*
  lightsClear();
  lightAdd(
    0, zz, -50,
    1.0, 0.0, 0.0
  );
  lightAdd(
    zz/2, zz/2, zz,
    1.0, 1.0, 1.0
  );
  */

  glDisable(GL_LIGHTING);
  if (doLights) {
    glShadeModel(GL_SMOOTH);
    __gshared float[4][8] ldiff = void;
    __gshared float[3][8] lpos = void;
    if (drawLights) {
      foreach (uint idx; 0..lightCount) {
        ldiff[idx] = lightColor.ptr[idx][];
        lpos[idx] = lightPos.ptr[idx][];
        glColor4f(ldiff[idx][0], ldiff[idx][1], ldiff[idx][2], 1.0);

        glBegin(/*GL_QUADS*/GL_TRIANGLE_FAN);
        glVertex3f(lpos[idx][0]-1, lpos[idx][1]-1, lpos[idx][2]);
        glVertex3f(lpos[idx][0]+1, lpos[idx][1]-1, lpos[idx][2]);
        glVertex3f(lpos[idx][0]+1, lpos[idx][1]+1, lpos[idx][2]);
        glVertex3f(lpos[idx][0]-1, lpos[idx][1]+1, lpos[idx][2]);
        glEnd();

        /*
        glPointSize(5);
        glBegin(GL_POINTS);
        glVertex3fv(lpos[idx].ptr);
        glEnd();
        glPointSize(1);
        */

        glColor3f(1, 1, 1);
      }
    }
    __gshared float[4] ambcol;
    ambcol[0] = 0.6f;
    ambcol[1] = 0.6f;
    ambcol[2] = 0.6f;
    ambcol[3] = 1.0f;
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambcol.ptr);
    foreach (uint idx; 0..lightCount) {
      ldiff[idx] = lightColor[idx][];
      lpos[idx] = lightPos[idx][];
      //{ import std.stdio; writeln("LIGHT #", idx, " at ", lightPos[idx][], " color ", lightColor[idx][]); }
      glEnable(GL_LIGHT0+idx);
      //glLightfv(GL_LIGHT0+idx, GL_DIFFUSE, &lightColor[idx][0]);
      //glLightfv(GL_LIGHT0+idx, GL_POSITION, &lightPos[idx][0]);
      assert((&ldiff[0])+1 == &ldiff[1]);
      glLightfv(GL_LIGHT0+idx, GL_DIFFUSE, ldiff[idx].ptr);
      //glLightfv(GL_LIGHT0+idx, GL_SPECULAR, ldiff[idx].ptr);
      glLightfv(GL_LIGHT0+idx, GL_POSITION, lpos[idx].ptr);
    }
    glEnable(GL_LIGHTING);
  }
  version(none) {
    /*
    static immutable float[4] ldiff0 = [1.0, 0.0, 0.0, 1.0];
    static immutable float[3] lpos0 = [-90.0, 0.0, 90.0];
    */
    __gshared float[4] ldiff0 = [1.0, 0.0, 0.0, 1.0];
    __gshared float[3] lpos0 = [-90.0, 0.0, 90.0];
    //lightColor[0] = ldiff0[];
    //lightPos[0] = lpos0[];
    //glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor[0].ptr);
    //glLightfv(GL_LIGHT0, GL_POSITION, lightPos[0].ptr);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, ldiff0.ptr);
    glLightfv(GL_LIGHT0, GL_POSITION, lpos0.ptr);
    //glEnable(GL_LIGHT0);
    //glEnable(GL_LIGHTING);
  }

  /*
  doLight(
    90, 40, -40,
    1.0, 0.0, 0.0
  );
  doLight(
    -90, 0, 90,
    1.0, 1.0, 1.0
  );
  */

  /*
  doLight(
    zz, zz/2, -zz/2,
    1.0, 0.0, 0.0
  );
  */
  /*
  doLight(
    0, zz, -50,
    1.0, 0.0, 0.0
  );
  doLight(
    zz/2, zz/2, zz,
    1.0, 1.0, 1.0
  );
  */

  //angle = 142;

  // rotate model
  glRotatef(angle, 0.8, 0.5, 0.3);
  /*
  glRotatef(angle, 0.8, 0.0, 0.0);
  glRotatef(angle, 0.0, 0.3, 0.0);
  glRotatef(angle, 0.0, 0.0, 0.1);
  */

  version(none) {
    // enable color tracking
    glEnable(GL_COLOR_MATERIAL);
    // set material properties which will be assigned by glColor
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    //glColorMaterial(GL_FRONT_AND_BACK, GL_SPECULAR);
    // blue reflective properties
    glColor3f(0.0f, 0.5f, 1.0f);
  }

  //glEnable(GL_LIGHTING);
  shipModel.glDraw();
  shipModel.glDrawExhaust();

  if (doLights) {
    foreach (uint idx; 0..lightCount) glDisable(GL_LIGHT0+idx);
    glDisable(GL_LIGHTING);
    glShadeModel(GL_FLAT);
  }
  //glDisable(GL_DEPTH_TEST);
}
