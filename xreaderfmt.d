/* Written by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module xreaderfmt;

import core.time;

import std.concurrency;

import iv.strex;

import arsd.simpledisplay;
import arsd.image;

import iv.cmdcon;
import iv.nanovega;
import iv.nanovega.blendish;
import iv.nanovega.textlayouter;
import iv.vfs;
import iv.vfs.io;

import booktext;

import xreadercfg;


// ////////////////////////////////////////////////////////////////////////// //
BookText loadBook (string fname) {
  __gshared bool eliteShipsLoaded = false;

  import std.path;
  //import core.memory : GC;
  fname = fname.expandTilde.absolutePath;
  //conwriteln("loading '", fname, "'...");
  //GC.disable();
  auto stt = MonoTime.currTime;
  auto book = new BookText(fname);
  //GC.enable();
  //GC.collect();
  { conwriteln("loaded: '", fname, "' in ", (MonoTime.currTime-stt).total!"msecs", " milliseconds"); }
  currentFileId = updateFileLastReadTime(fname, book.getAuthor(), book.getTitle(),
                                         book.sequence, book.seqnum);

  if (!eliteShipsLoaded) {
    loadEliteShips();
    eliteShipsLoaded = true;
  }

  return book;
}


// ////////////////////////////////////////////////////////////////////////// //
private struct ImageInfo {
  enum MaxIdleFrames = 5;
  NVGImage iid;
  TrueColorImage img;
  int framesNotUsed;
}

private __gshared ImageInfo[] nvgImageIds;


void releaseImages (NVGContext vg) {
  if (nvgImageIds.length) {
    foreach (ref ImageInfo nfo; nvgImageIds) {
      if (nfo.iid.valid) {
        if (++nfo.framesNotUsed > ImageInfo.MaxIdleFrames) {
          //conwriteln("freeing image with id ", nfo.iid);
          vg.deleteImage(nfo.iid);
          nfo.img = null;
        }
      }
    }
  }
}


private NVGImage registerImage (NVGContext vg, TrueColorImage img) {
  if (vg is null || img is null) return NVGImage.init;
  uint freeIdx = uint.max;
  foreach (immutable idx, ref ImageInfo nfo; nvgImageIds) {
    if (nfo.img is img) {
      assert(nfo.iid.valid);
      nfo.framesNotUsed = 0;
      return nfo.iid;
    }
    if (freeIdx != uint.max && !nfo.iid.valid) freeIdx = cast(uint)idx;
  }
  NVGImage iid = vg.createImageFromMemoryImage(img); //vg.createImageRGBA(img.width, img.height, img.imageData.bytes[], NVGImageFlags.None);
  if (!iid.valid) return NVGImage.init;
  nvgImageIds ~= ImageInfo(iid, img, 0);
  return iid;
}


class BookImage : LayObject {
  BookText book;
  uint imageidx;
  TrueColorImage img;
  this (BookText abook, uint aidx) { book = abook; imageidx = aidx; img = book.images[aidx].img; }
  override int width () { return img.width; }
  override int spacewidth () { return 0; }
  override int height () { return img.height; }
  override int ascent () { return img.height; }
  override int descent () { return 0; }
  override bool canbreak () { return true; }
  override bool spaced () { return false; }
  override void draw (NVGContext vg, float x, float y) {
    y -= img.height;
    vg.save();
    scope(exit) vg.restore();
    vg.beginPath();
    NVGImage iid = vg.registerImage(img);
    if (!iid.valid) return;
    vg.fillPaint(vg.imagePattern(x, y, img.width, img.height, 0, iid));
    //vg.globalAlpha(0.5);
    vg.rect(x, y, img.width, img.height);
    vg.fill();
  }
}


// ////////////////////////////////////////////////////////////////////////// //
class BookMetadata {
  static struct Anc {
    dstring name;
    uint wordidx; // first word
  }
  Anc[] sections;
  Anc[uint] hrefs; // `name` is dest
  Anc[] ids;
}


// ////////////////////////////////////////////////////////////////////////// //
final class FBFormatter {
private:
  BookText book;
  bool hasSections;

public:
  LayTextC lay;
  BookMetadata meta;

public:
  this () {}

  void formatBook (BookText abook, int maxWidth) {
    assert(abook !is null);

    lay = new LayTextC(laf, maxWidth);
    lay.fontStyle.color = colorText.asUint;

    meta = new BookMetadata();

    book = abook;
    scope(exit) book = null;
    hasSections = false;

    putMain();

    lay.finalize();
  }

private:
  void dumpTree (Tag ct) {
    while (ct !is null) {
      conwriteln("tag: ", ct.name);
      ct = ct.parent;
    }
  }

  void badTag (Tag tag, string msg=null) {
    import std.string : format, indexOf;
    assert(tag !is null);
    dumpTree(tag);
    if (msg.length == 0) msg = "invalid tag: '%s'";
    if (msg.indexOf("%s") >= 0) {
      throw new Exception(msg.format(tag.name));
    } else {
      throw new Exception(msg);
    }
  }

  void saveTagId (Tag tag) {
    if (tag.id.length) {
      import std.conv : to;
      meta.ids ~= BookMetadata.Anc(tag.id.to!dstring, lay.nextWordIndex);
    }
  }

  void registerSection (Tag tag) {
    import std.conv : to;
    string text = tag.textContent.xstrip;
    //lay.sections ~= lay.curWordIndex;
    string name;
    while (text.length) {
      char ch = text.ptr[0];
      text = text[1..$];
      if (ch <= ' ' || ch == 127) {
        if (name.length == 0) continue;
        if (ch != '\n') ch = ' ';
        if (ch == '\n') {
          if (name[$-1] > ' ') name ~= "\n...";
          text = text.xstrip;
        } else {
          if (name[$-1] > ' ') name ~= ' ';
        }
      } else {
        name ~= ch;
      }
    }
    name = xstrip(name);
    if (name.length == 0) name = "* * *";
    meta.sections ~= BookMetadata.Anc(name.to!dstring, lay.nextWordIndex);
  }

  void putImage (Tag tag) {
    if (tag is null || tag.href.length < 2 || tag.href[0] != '#') return;
    string iid = tag.href[1..$];
    //conwriteln("searching for image with href '", iid, "' (", book.images.length, ")");
    foreach (immutable idx, ref BookText.Image img; book.images) {
      if (iid == img.id) {
        //conwriteln("image '", img.id, "' found");
        lay.putObject(new BookImage(book, cast(uint)idx));
        return;
      }
    }
  }

  void putOneTag(bool allowBad=false, bool allowText=true) (Tag tag) {
    if (tag is null) return;
    if (tag.name.length == 0) {
      static if (allowText) {
        saveTagId(tag);
        lay.put(tag.text);
      }
      return;
    }
    switch (tag.name) {
      case "p":
        putTagContents(tag);
        lay.endPara();
        break;

      case "strong":
        lay.pushStyles();
        scope(exit) lay.popStyles;
        lay.fontStyle.bold = true;
        putTagContents(tag);
        break;
      case "emphasis":
        lay.pushStyles();
        scope(exit) lay.popStyles;
        lay.fontStyle.italic = true;
        //fixFont();
        putTagContents(tag);
        break;
      case "strikethrough":
        lay.pushStyles();
        scope(exit) lay.popStyles;
        lay.fontStyle.strike = true;
        //fixFont();
        putTagContents(tag);
        break;

      case "empty-line":
        lay.endLine();
        break;
      case "br":
        lay.endPara();
        break;

      case "image":
        lay.pushStyles();
        scope(exit) lay.popStyles;
        lay.endLine;
        lay.lineStyle.setCenter;
        putImage(tag);
        lay.endLine;
        return;

      case "style":
        //???
        return;

      case "a":
        if (tag.href.length) {
          import std.conv : to;
          //conwriteln("href found: '", ct.href, "' (", lay.nextWordIndex, ")");
          meta.hrefs[lay.nextWordIndex] = BookMetadata.Anc(tag.href.to!dstring, lay.nextWordIndex);
          lay.pushStyles();
          auto c = lay.fontStyle.color;
          scope(exit) { lay.popStyles; lay.fontStyle.color = c; }
          lay.fontStyle.href = true;
          lay.fontStyle.underline = true;
          lay.fontStyle.color = colorTextHref.asUint;
          //fixFont();
          putTagContents(tag);
        } else {
          putTagContents(tag);
        }
        break;

      case "sub": // some idiotic files has this
      case "sup": // some idiotic files has this
        putTagContents(tag);
        break;

      case "code":
        lay.pushStyles();
        scope(exit) lay.popStyles;
        setCodeStyle();
        putTagContents(tag);
        lay.endPara;
        break;

      case "poem":
        lay.pushStyles();
        scope(exit) lay.popStyles;
        putPoem(tag);
        //lay.endPara;
        break;

      case "stanza":
        putTagContents(tag);
        lay.endPara;
        break;

      case "v": // for stanzas
        putTagContents(tag);
        lay.endPara;
        break;

      case "date": // for poem
        lay.endPara;
        lay.pushStyles();
        scope(exit) lay.popStyles;
        lay.fontStyle.bold = true;
        lay.fontStyle.italic = true;
        lay.lineStyle.setRight;
        //fixFont();
        putTagContents(tag);
        lay.endPara;
        break;

      case "cite":
        putCite(tag);
        return;

      case "site":
      case "annotation":
        lay.pushStyles();
        scope(exit) lay.popStyles;
        lay.fontStyle.bold = true;
        lay.lineStyle.setCenter;
        //fixFont();
        putTagContents(tag);
        lay.endPara;
        break;

      case "text-author":
        break;

      case "title":
        lay.pushStyles();
        scope(exit) lay.popStyles;
        setTitleStyle();
        registerSection(tag);
        putTagContents(tag);
        if (tag.text.length == 0) lay.endPara();
        break;

      case "subtitle":
        lay.pushStyles();
        scope(exit) lay.popStyles;
        setSubtitleStyle();
        putTagContents(tag);
        if (tag.text.length == 0) lay.endPara();
        break;

      case "epigraph":
        lay.pushStyles();
        scope(exit) lay.popStyles;
        //auto olpad = lay.lineStyle.leftpad;
        auto orpad = lay.lineStyle.rightpad;
        setEpigraphStyle();
        //lay.lineStyle.leftpad = lay.lineStyle.leftpad+olpad;
        lay.lineStyle.rightpad = lay.lineStyle.rightpad+orpad;
        putEpigraph(tag);
        break;

      case "section":
        putTagContents(tag);
        lay.endPara;
        break;

      case "table":
        lay.pushStyles();
        scope(exit) lay.popStyles;
        lay.fontStyle.fontsize += 8;
        lay.lineStyle.setCenter;
        //fixFont();
        lay.put("TABLE SKIPPED");
        lay.endPara();
        break;

      default:
        static if (allowBad) {
          break;
        } else {
          badTag(tag);
        }
    }
  }

  bool onlyImagePara (Tag ct) {
    if (ct is null) return false;
    int count;
    foreach (Tag tag; ct.children) {
      if (tag.name.length == 0) {
        // text
        if (tag.text.xstrip.length != 0) return false;
        continue;
      }
      if (count != 0 || tag.name != "image") return false;
      ++count;
    }
    return (count == 1);
  }

  void putTagContents/*(bool xdump=false)*/ (Tag ct) {
    if (ct is null) return;
    saveTagId(ct);
    if (onlyImagePara(ct)) {
      lay.pushStyles();
      scope(exit) lay.popStyles;
      lay.endLine;
      lay.lineStyle.setCenter;
      foreach (Tag tag; ct.children) putOneTag(tag);
      lay.endLine;
    } else {
      //if (ct.name == "subtitle") conwriteln("text: [", ct.children[0].text, "]");
      foreach (Tag tag; ct.children) {
        //static if (xdump) conwriteln("text: [", tag.text, "]");
        putOneTag(tag);
      }
    }
    //lay.endPara();
  }

  void putParas (Tag ct) {
    if (ct is null) return;
    putTagContents(ct);
    lay.endPara();
  }

  void putAuthor (Tag ct) {
    saveTagId(ct);
    foreach (Tag tag; ct.children) {
      if (tag.name.length == 0) continue;
      if (tag.name == "text-author") {
        putTagContents(tag);
        lay.endPara();
      }
    }
  }

  void putCite (Tag ct) {
    lay.pushStyles();
    scope(exit) lay.popStyles;
    setCiteStyle();
    putParas(ct);
    putAuthor(ct);
  }

  void putEpigraph (Tag ct) {
    putParas(ct);
    putAuthor(ct);
  }

  void putPoem (Tag ct) {
    saveTagId(ct);
    lay.pushStyles();
    scope(exit) lay.popStyles;
    setPoemStyle();
    // put epigraph (not yet)
    // put title and subtitle
    foreach (Tag tag; ct.children) {
      if (tag.name == "title") {
        lay.pushStyles();
        scope(exit) lay.popStyles;
        if (!hasSections) registerSection(tag);
        putParas(tag);
        lay.endPara(); // space
      } else if (tag.name == "subtitle") {
        lay.pushStyles();
        scope(exit) lay.popStyles;
        putParas(tag);
        //lay.endPara(); // space
      }
    }
    lay.endPara;
    foreach (Tag tag; ct.children) {
      putOneTag!(false, false)(tag); // skip text without tags
    }
    putAuthor(ct);
  }

  void putMain () {
    setNormalStyle();
    foreach (Tag tag; book.content.children) {
      putOneTag(tag);
    }
  }

private:
  static public void fixFont (LayFontStash laf, ref LayFontStyle st) nothrow @safe @nogc {
    if (st.monospace) {
      if (st.italic) {
        if (st.bold) st.fontface = laf.fontFaceId("monoz");
        else st.fontface = laf.fontFaceId("monoi");
      } else if (st.bold) {
        if (st.italic) st.fontface = laf.fontFaceId("monoz");
        else st.fontface = laf.fontFaceId("monob");
      } else {
        st.fontface = laf.fontFaceId("mono");
      }
    } else {
      if (st.italic) {
        if (st.bold) st.fontface = laf.fontFaceId("textz");
        else st.fontface = laf.fontFaceId("texti");
      } else if (st.bold) {
        if (st.italic) st.fontface = laf.fontFaceId("textz");
        else st.fontface = laf.fontFaceId("textb");
      } else {
        st.fontface = laf.fontFaceId("text");
      }
    }
  }

  void setNormalStyle () {
    lay.fontStyle.resetAttrs;
    lay.fontStyle.fontsize = fsizeText;
    lay.fontStyle.color = colorText.asUint;
    lay.lineStyle.leftpad = 0;
    lay.lineStyle.rightpad = 0;
    lay.lineStyle.paraIndent = 3;
    if (optJustify) lay.lineStyle.setJustify; else lay.lineStyle.setLeft;
    //fixFont();
  }

  void setTitleStyle () {
    lay.fontStyle.resetAttrs;
    lay.fontStyle.bold = true;
    lay.fontStyle.fontsize = fsizeText+4;
    lay.fontStyle.color = colorText.asUint;
    lay.lineStyle.leftpad = 0;
    lay.lineStyle.rightpad = 0;
    lay.lineStyle.paraIndent = 0;
    lay.lineStyle.setCenter;
    //fixFont();
  }

  void setSubtitleStyle () {
    lay.fontStyle.resetAttrs;
    lay.fontStyle.bold = true;
    lay.fontStyle.fontsize = fsizeText+2;
    lay.fontStyle.color = colorText.asUint;
    lay.lineStyle.leftpad = 0;
    lay.lineStyle.rightpad = 0;
    lay.lineStyle.paraIndent = 0;
    lay.lineStyle.setCenter;
    //fixFont();
  }

  void setCiteStyle () {
    lay.fontStyle.resetAttrs;
    lay.fontStyle.italic = true;
    lay.fontStyle.fontsize = fsizeText;
    lay.fontStyle.color = colorText.asUint;
    lay.lineStyle.leftpad = fsizeText*3;
    lay.lineStyle.rightpad = fsizeText*3;
    lay.lineStyle.paraIndent = 3;
    if (optJustify) lay.lineStyle.setJustify; else lay.lineStyle.setLeft;
    //fixFont();
  }

  void setEpigraphStyle () {
    lay.fontStyle.resetAttrs;
    lay.fontStyle.italic = true;
    lay.fontStyle.fontsize = fsizeText;
    lay.fontStyle.color = colorText.asUint;
    lay.lineStyle.leftpad = lay.width/3;
    lay.lineStyle.rightpad = 0;
    lay.lineStyle.paraIndent = 0;
    lay.lineStyle.setRight;
    //fixFont();
  }

  void setPoemStyle () {
    lay.fontStyle.resetAttrs;
    lay.fontStyle.italic = true;
    lay.fontStyle.fontsize = fsizeText;
    lay.fontStyle.color = colorText.asUint;
    lay.lineStyle.leftpad = fsizeText*3;
    lay.lineStyle.rightpad = 0;
    lay.lineStyle.paraIndent = 0;
    lay.lineStyle.setLeft;
    //fixFont();
  }

  void setCodeStyle () {
    lay.fontStyle.resetAttrs;
    lay.fontStyle.monospace = true;
    lay.fontStyle.italic = false;
    lay.fontStyle.fontsize = fsizeText;
    lay.fontStyle.color = colorText.asUint;
    lay.lineStyle.leftpad = fsizeText*3;
    lay.lineStyle.rightpad = fsizeText*3;
    lay.lineStyle.paraIndent = 0;
    lay.lineStyle.setLeft;
    //lay.fontStyle.fontface = lay.fontFaceId("mono");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
private __gshared LayFontStash laf; // layouter font stash


// ////////////////////////////////////////////////////////////////////////// //
private void loadFmtFonts (/*NVGContext nvg*/) {
  import std.functional : toDelegate;
  if (laf is null) {
    //if (nvg !is null) { import core.stdc.stdio : printf; printf("reusing NVG context...\n"); }
    laf = new LayFontStash(/*nvg*/);
    laf.fixFontDG = toDelegate(&FBFormatter.fixFont);

    laf.addFont("text", textFontName);
    laf.addFont("texti", textiFontName);
    laf.addFont("textb", textbFontName);
    laf.addFont("textz", textzFontName);

    laf.addFont("mono", monoFontName);
    laf.addFont("monoi", monoiFontName);
    laf.addFont("monob", monobFontName);
    laf.addFont("monoz", monozFontName);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// messages
struct ReformatWork {
  shared(BookText) booktext;
  //shared(NVGContext) nvg; // can be null
  string bookFileName;
  int w, h;
}

struct ReformatWorkComplete {
  int w, h;
  shared(BookText) booktext;
  shared(LayTextC) laytext;
  shared(BookMetadata) meta;
}

struct QuitWork {
}


// ////////////////////////////////////////////////////////////////////////// //
void reformatThreadFn (Tid ownerTid) {
  bool doQuit = false;
  BookText book;
  string newFileName;
  int newW = -1, newH = -1;
  //NVGContext lastnvg = null;
  while (!doQuit) {
    book = null;
    newFileName = null;
    newW = newH = -1;
    receive(
      (ReformatWork w) {
        //{ conwriteln("reformat request received..."); }
        book = cast(BookText)w.booktext;
        newFileName = w.bookFileName;
        newW = w.w;
        newH = w.h;
        if (newW < 1) newW = 1;
        if (newH < 1) newH = 1;
        //lastnvg = cast(NVGContext)w.nvg;
      },
      (QuitWork w) {
        doQuit = true;
      },
    );
    if (!doQuit && newW > 0 && newH > 0) {
      try {
        if (book is null) {
          //conwriteln("loading new book: '", newFileName, "'");
          book = loadBook(newFileName);
        }

        int maxWidth = newW-4-2-BND_SCROLLBAR_WIDTH-2;
        if (maxWidth < 64) maxWidth = 64;

        loadFmtFonts();

        // layout text
        //conwriteln("layouting...");
        auto fmtr = new FBFormatter();

        auto stt = MonoTime.currTime;
        /*
        auto lay = new LayText(laf, maxWidth);
        lay.fontStyle.color = colorText.asUint;
        auto meta = book.formatBook(lay);
        */
        fmtr.formatBook(book, maxWidth);
        auto ett = MonoTime.currTime-stt;

        auto meta = fmtr.meta;
        auto lay = fmtr.lay;

        conwriteln("layouted in ", ett.total!"msecs", " milliseconds");
        auto res = ReformatWorkComplete(newW, newH, cast(shared)book, cast(shared)lay, cast(shared)meta);
        send(ownerTid, res);
      } catch (Throwable e) {
        // here, we are dead and fucked (the exact order doesn't matter)
        import core.stdc.stdlib : abort;
        import core.stdc.stdio : fprintf, stderr;
        import core.memory : GC;
        import core.thread : thread_suspendAll;
        GC.disable(); // yeah
        thread_suspendAll(); // stop right here, you criminal scum!
        auto s = e.toString();
        fprintf(stderr, "\n=== FATAL ===\n%.*s\n", cast(uint)s.length, s.ptr);
        abort(); // die, you bitch!
      }
    }
  }
  send(ownerTid, QuitWork());
}
