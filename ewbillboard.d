/* Written by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module ewbillboard;

import iv.cmdcon;
import iv.nanovega;
import iv.nanovega.blendish;
//import arsd.color;

import xreadercfg;

import eworld;


// ////////////////////////////////////////////////////////////////////////// //
__gshared Galaxy universe;


// ////////////////////////////////////////////////////////////////////////// //
final class PlanetInfoBillboard {
private:
  string[2][] lines;
  float[] lineH; // line height
  float[2] ltblW = 0;

  string pdsc;
  float pdscH;

  string[3][] market;
  float[] mlineH;
  float[3] mtblW = 0; // market widthes

  float billH = 0, billW = 0;
  NVGContext vg;

public:
  this (NVGContext avg, ref Galaxy.Planet p) {
    assert(avg !is null);
    vg = avg;
    scope(exit) vg = null;
    vg.fontFaceId(galmapFont);
    vg.textAlign(NVGTextAlign.H.Left, NVGTextAlign.V.Baseline);
    vg.fontSize(fGalMapSize);
    vg.textAlign(NVGTextAlign.H.Left, NVGTextAlign.V.Top);
    buildPlanetData(p);
  }

  void draw (NVGContext avg, float mx=10, float my=10) {
    if (avg is null) return;
    vg = avg;
    scope(exit) vg = null;

    vg.save();
    scope(exit) vg.restore;

    auto obfn = bndGetFont();
    scope(exit) bndSetFont(obfn);

    vg.resetTransform();
    vg.resetScissor;

    version(none) {
      vg.fontFaceId(uiFont);
      vg.textAlign(NVGTextAlign.H.Left, NVGTextAlign.V.Top);
      vg.fontSize(16);
      float[4] b = void;
      float adv;
      adv = vg.textBounds(0, 0, "Woo", b[]);
      conwriteln("adv=", adv, "; b=", b[]);
    }

    bndSetFont(galmapFont);
    vg.fontFaceId(galmapFont);
    vg.textAlign(NVGTextAlign.H.Left, NVGTextAlign.V.Baseline);
    vg.fontSize(fGalMapSize);

    vg.bndMenuBackground(mx, my, billW+8, billH+8, BND_CORNER_NONE);
    vg.beginPath();
    float y = my+4;
    // planet info
    foreach (immutable idx; 0..lines.length) {
      float x = mx+4;
      vg.fillColor(nvgRGBA(255, 255, 255));
      vg.textAlign(NVGTextAlign.H.Right, NVGTextAlign.V.Top);
      x += ltblW[0];
      vg.text(x, y, lines[idx][0]);
      x += 4;
      vg.fillColor(nvgRGBA(255, 255, 0));
      vg.textAlign(NVGTextAlign.H.Left, NVGTextAlign.V.Top);
      vg.text(x, y, lines[idx][1]);
      y += lineH[idx];
    }
    // planet descrition
    vg.fillColor(nvgRGBA(255, 127, 0));
    vg.textAlign(NVGTextAlign.H.Left, NVGTextAlign.V.Top);
    y += 8;
    vg.text(mx+4, y, pdsc);
    y += pdscH+4;
    // market data
    vg.fillColor(nvgRGBA(255, 255, 255));
    foreach (immutable idx; 0..market.length) {
      float x = mx+4;
      vg.textAlign(NVGTextAlign.H.Left, NVGTextAlign.V.Top);
      vg.text(x, y, market[idx][0]);
      x += mtblW[0]+8+mtblW[1];
      vg.textAlign(NVGTextAlign.H.Right, NVGTextAlign.V.Top);
      vg.text(x, y, market[idx][1]);
      x += mtblW[2]+8;
      vg.text(x, y, market[idx][2]);
      vg.textAlign(NVGTextAlign.H.Left, NVGTextAlign.V.Top);
      x += 1;
      vg.text(x, y, Galaxy.good(idx).unitName);
      y += mlineH[idx];
    }
    vg.fill();
  }

private:
  void putf(A...) (string label, string fmt, in A args) {
    import std.algorithm : max;
    import std.string : format;
    auto s = format(fmt, args);
    lines.length += 1;
    lines[$-1][0] = label;
    lines[$-1][1] = s;
    float[4] b = void;
    // label
    vg.textBounds(0, 0, label, b[]);
    float hh = b[3]-b[1];
    float ww = b[2]-b[0];
    ltblW[0] = max(ltblW[0], ww);
    // text
    vg.textBounds(0, 0, s, b[]);
    hh = max(hh, b[3]-b[1]);
    ww = b[2]-b[0];
    ltblW[1] = max(ltblW[1], ww);
    // totals
    lineH ~= hh;
    billH += hh;
    billW = max(billW, ltblW[0]+4+ltblW[1]);
  }

  void putd (string dsc) {
    import std.algorithm : max;
    pdsc = dsc;
    float[4] b = void;
    vg.textBounds(0, 0, dsc, b[]);
    float hh = b[3]-b[1];
    float ww = b[2]-b[0];
    // totals
    pdscH = hh;
    billH += hh;
    billW = max(billW, ww);
  }

  void putm(T : ulong) (in ref Galaxy.Planet p, T idx) {
    import std.algorithm : max;
    import std.string : format;
    if (idx < 0 || idx > Galaxy.Goods.max) return;
    auto good = Galaxy.good(idx);
    market.length += 1;
    market[$-1][0] = good.name;
    market[$-1][1] = "%s.%s".format(p.price[idx]/10, p.price[idx]%10);
    market[$-1][2] = "%s".format(p.quantity[idx]);
    float[4] b = void;
    float hh = 0, wt = 8*2;
    foreach (immutable n, string s; market[$-1]) {
      vg.textBounds(0, 0, s, b[]);
      hh = max(hh, b[3]-b[1]);
      mtblW[n] = max(mtblW[n], b[2]-b[0]);
      wt += mtblW[n];
    }
    vg.textBounds(0, 0, "kg", b[]);
    hh = max(hh, b[3]-b[1]);
    wt += b[2]-b[0]+2;
    mlineH ~= hh;
    billH += hh;
    billW = max(billW, wt);
  }

  void buildPlanetData (ref Galaxy.Planet p) {
    putf("System:", "%s", p.name);
    putf("Position:", "(%s,%s)", p.x, p.y);
    putf("Economy:", "%s", p.economyName);
    putf("Government:", "%s", p.govName);
    putf("Tech Level:", "%s", p.techlev+1);
    putf("Turnover:", "%s", p.productivity);
    putf("Radius:", "%s", p.radius);
    putf("Population:", "%s.%s Billion", p.population/10, p.population%10);
    putd(p.description);
    billH += 16;
    foreach (immutable idx; 0..Galaxy.Goods.max+1) putm(p, idx);
    version(none) {
      float[4] b;
      float a;
      a = vg.textBounds(0, 0, "m", b[]);
      conwriteln(a, " : ", b[]);
      a = vg.textBounds(0, 0, "o", b[]);
      conwriteln(a, " : ", b[]);
      a = vg.textBounds(0, 0, "mo", b[]);
      conwriteln(a, " : ", b[]);
      debug(nanovg_fons_kern_test) NVGKTT(vg);
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared ulong lastGalSeed = 0;
__gshared ubyte lastPlanetNum = 0;
__gshared PlanetInfoBillboard planetBill;


void drawGalaxy (NVGContext vg) {
  if (vg is null) return;

  if (planetBill is null || lastGalSeed != universe.galSeed || lastPlanetNum != universe.currPlanet.number) {
    lastGalSeed = universe.galSeed;
    lastPlanetNum = universe.currPlanet.number;
    planetBill.destroy;
    planetBill = new PlanetInfoBillboard(vg, universe.currPlanet);
  }

  vg.save();
  scope(exit) vg.restore;
  vg.resetScissor;

  vg.fontFaceId(uiFont);
  vg.textAlign(NVGTextAlign.H.Left, NVGTextAlign.V.Baseline);
  vg.fontSize(fsizeUI);

  //vg.beginFrame(GWidth, GHeight, 1);
  //vg.scissor(0, 0, GWidth, GHeight);
  //vg.resetTransform();
  //vg.resetScissor;
  //vg.translate((GWidth-256*2-8)/2, (GHeight-256*2-8)/2);

  float mx = (GWidth-256*2-8)/2.0;
  float my = (GHeight-256*2-8)/1.0-8;

  vg.bndMenuBackground(mx, my, 256*2+8, 256*2+8, BND_CORNER_NONE);

  vg.beginPath();
  vg.fillColor(nvgRGBA(255, 255, 255));
  foreach (const ref p; universe.planets) {
    vg.circle(mx+p.x*2+4, my+p.y*2+4, 1);
  }
  vg.fill();

  vg.beginPath();
  vg.strokeColor(nvgRGBA(255, 255, 0));
  vg.circle(mx+universe.currPlanet.x*2+4, my+universe.currPlanet.y*2+4, 4);
  vg.stroke();

  // show planet data
  planetBill.draw(vg);

  //vg.endFrame();
}
