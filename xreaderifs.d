/* Written by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module xreaderifs;

import arsd.simpledisplay;
import arsd.image;

import iv.nanovega;

import xreadercfg;


// ////////////////////////////////////////////////////////////////////////// //
struct Interference {
  int y, ey, speed;
}

__gshared Interference[] ifs;


// ////////////////////////////////////////////////////////////////////////// //
bool processIfs () {
  bool res = false;
  foreach (ref l; ifs) {
    if (l.speed == 0) continue;
    res = true;
    l.y += l.speed;
    if (l.speed < 0) {
      if (l.y < l.ey) l.speed = 0;
    } else {
      if (l.y > l.ey) l.speed = 0;
    }
  }
  return res;
}


void drawIfs (NVGContext vg) {
  if (vg is null) return;
  foreach (ref l; ifs) {
    if (l.speed == 0) continue;
    vg.beginPath();
    vg.fillColor(nvgRGB(0, 40, 0));
    vg.rect(0, l.y-1, GWidth, 3);
    vg.fill();
  }
}


bool addIf () {
  import std.random : uniform;
  if (interAllowed) {
    int idx = 0;
    while (idx < ifs.length && ifs[idx].speed != 0) ++idx;
    if (idx >= ifs.length) {
      if (ifs.length > 10) return false;
      ifs ~= Interference();
    }
    if (uniform!"[]"(0, 1) == 0) {
      // up
      ifs[idx].y = GHeight/2+uniform!"[]"(50, GHeight/2-100);
      int ty = ifs[idx].y-40;
      if (ty < 0) return false;
      ifs[idx].ey = uniform!"[]"(0, ty);
      ifs[idx].speed = -uniform!"[]"(1, 25);
    } else {
      // down
      ifs[idx].y = GHeight/2-uniform!"[]"(50, GHeight/2-100);
      int ty = ifs[idx].y+40;
      if (ty >= GHeight) return false;
      ifs[idx].ey = uniform!"[]"(ty, GHeight);
      ifs[idx].speed = uniform!"[]"(1, 25);
    }
    return true;
  }
  return false;
}
