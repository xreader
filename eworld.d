/* based on txtelite.c v1.5
 * Textual version of Elite trading
 * Converted by Ian Bell from 6502 Elite sources.
 * Original 6502 Elite by Ian Bell & David Braben.
 * Edited by Richard Carlsson to compile cleanly under gcc
 * and to fix a bug in the goat soup algorithm.
 *
 * Ported to D by Ketmar // Invisible Vector
 */
/* ----------------------------------------------------------------------------
 * The nature of basic mechanisms used to generate the Elite socio-economic
 * universe are now widely known. A competant games programmer should be able to
 * produce equivalent functionality. A competant hacker should be able to lift
 * the exact system from the object code base of official conversions.
 *
 * This file may be regarded as defining the Classic Elite universe.

 * It contains a C implementation of the precise 6502 algorithms used in the
 * original BBC Micro version of Acornsoft Elite together with a parsed textual
 * command testbed.
 *
 * Note that this is not the universe of David Braben's 'Frontier' series.
 *
 * ICGB 13/10/99
 * iancgbell@email.com
 * www.ibell.co.uk
 * ---------------------------------------------------------------------------- */
module eworld;

import iv.cmdcon;


// ////////////////////////////////////////////////////////////////////////// //
public struct Galaxy {
  // some numbers for galaxy 1
  enum Lave = 7;
  enum Zaonce = 129;
  enum Diso = 147;
  enum Ried = 46;

  enum PlanetsInGalaxy = 256;

  enum Goods : ubyte {
    Food = 0,
    Textiles = 1,
    Radioactives = 2,
    Slaves = 3,
    LiquorWines = 4,
    Luxuries = 5,
    Narcotics = 6,
    Computers = 7,
    Machinery = 8,
    Alloys = 9,
    Firearms = 10,
    Furs = 11,
    Minerals = 12,
    Gold = 13,
    Platinum = 14,
    GemStones = 15,
    AlienItems = 16,
  }

  enum Unit { Tonne, Kg, G }

  static struct TradeGood {
    string name;
    string unitName; // unit name
    Unit unit;
    bool alien; // alien items?
    bool forbidden; // narcotics or alike?
  }

  static TradeGood good(T : ulong) (T idx) {
    if (idx > Goods.max) assert(0, "invalid trade good index");
    TradeGood res;
    res.name = commodities[idx].name;
    res.unitName = unitnames[commodities[idx].units];
    res.unit = cast(Unit)commodities[idx].units;
    res.alien = (idx == Galaxy.Goods.AlienItems);
    res.forbidden = (idx == Galaxy.Goods.Slaves || idx == Galaxy.Goods.Narcotics);
    return res;
  }

  enum Gov : ubyte {
    Anarchy,
    Feudal,
    MultiGov,
    Dictatorship,
    Communist,
    Confederacy,
    Democracy,
    CorporateState,
  }

  enum Economy : ubyte {
    RichInd,
    AverageInd,
    PoorInd,
    MainlyInd,
    MainlyAgri,
    RichAgri,
    AverageAgri,
    PoorAgri,
  }

  enum MaxTechLevel = 16;

  string economyName(T : ulong) (T v) const pure nothrow @safe @nogc { return (v >= 0 && v <= Economy.max ? econnames[economy] : "Unknown"); }
  string govName(T : ulong) (T v) const pure nothrow @safe @nogc { return (v >= 0 && v <= Gov.max ? govnames[govtype] : "Unknown"); }

  static struct Planet {
    ubyte number; // in galaxy
    ubyte x, y;
    Economy economy; // 0..7
    Gov govtype; // 0..7
    ubyte techlev; // 0..16
    ubyte population;
    ushort productivity;
    ushort radius;
    string name;

    ushort lastFluct; // fluct used in last genMarket call

    private FastPRng goatsoupseed;
    private string desc;

    // market; please, generate it before using! ;-)
    ushort[Goods.max+1] quantity;
    ushort[Goods.max+1] price;

    string economyName () const pure nothrow @safe @nogc { return econnames[economy]; }
    string govName () const pure nothrow @safe @nogc { return govnames[govtype]; }

    /* Prices and availabilities are influenced by the planet's economy type
     * (0-7) and a random "fluctuation" byte that was kept within the saved
     * commander position to keep the market prices constant over gamesaves.
     * Availabilities must be saved with the game since the player alters them
     * by buying (and selling(?))
     *
     * Almost all operations are one byte only and overflow "errors" are
     * extremely frequent and exploited.
     *
     * Trade Item prices are held internally in a single byte=true value/4.
     * The decimal point in prices is introduced only when printing them.
     * Internally, all prices are integers.
     * The player's cash is held in four bytes.
     */
    void genMarket (ushort fluct) {
      lastFluct = fluct;
      foreach (uint i; 0..Galaxy.Goods.max+1) {
        int product = economy*commodities[i].gradient;
        int changing = fluct&commodities[i].maskbyte;
        int q = commodities[i].basequant+changing-product;
        q = q&0xFF;
        if (q&0x80) q = 0; // clip to positive 8-bit
        quantity[i] = cast(ushort)(q&0x3F); // mask to 6 bits
        q = commodities[i].baseprice+changing+product;
        q = q&0xFF;
        price[i] = cast(ushort)(q*4);
      }
      quantity[Galaxy.Goods.AlienItems] = 0; // override to force nonavailability
    }

    // seperation between two planets (4*sqrt(X*X+Y*Y/4))
    int distanceTo() (in auto ref Planet b) const nothrow @safe @nogc {
      import std.math : floor, sqrt;
      return cast(int)floor((4.0*sqrt((this.x-b.x)*(this.x-b.x)+(this.y-b.y)*(this.y-b.y)/4.0))+0.5);
    }

    // goat soup
    string description () {
      if (desc.length == 0) {
        FastPRng prng = goatsoupseed;
        desc = goatSoup(prng, "\x8F is \x97.", this);
      }
      return desc;
    }
  }

  Planet[PlanetsInGalaxy] planets;
  private GalaxySeed galseed;
  bool initialized;
  Planet currPlanet; // current planet
  private ulong curGalSeed;

  this (ubyte galnum) { build(galnum); }

  @property ulong galSeed () const pure nothrow @nogc { return curGalSeed; }
  @property void galSeed (ulong v) pure nothrow @nogc { curGalSeed = v; }

  // generate galaxy
  void build (ubyte galnum) {
    galseed = GalaxySeed.galaxy(galnum);
    curGalSeed = galseed.seed;
    foreach (immutable idx, ref Planet ps; planets) { makeSystem(ps); ps.number = cast(ubyte)idx; }
    initialized = true;
    // generate data for Lave (ignore the fact that we can be in another galaxy)
    currPlanet = planets[Lave];
    currPlanet.genMarket(0);
  }

  // regenerate galaxy
  void rebuild () {
    galseed.seed = curGalSeed;
    foreach (immutable idx, ref Planet ps; planets) { makeSystem(ps); ps.number = cast(ubyte)idx; }
    initialized = true;
    currPlanet = planets[currPlanet.number];
    currPlanet.genMarket(randbyte());
  }

  void hyperjump () {
    galseed.seed = curGalSeed;
    galseed.nextgalaxy();
    curGalSeed = galseed.seed;
    foreach (immutable idx, ref Planet ps; planets) { makeSystem(ps); ps.number = cast(ubyte)idx; }
    initialized = true;
    // generate data for Lave (ignore the fact that we can be in another galaxy)
    currPlanet = planets[currPlanet.number];
    currPlanet.genMarket(randbyte());
  }

  // move to system i
  bool jump (int i) {
    if (i >= 0 && i < planets.length) {
      currPlanet = planets[i];
      currPlanet.genMarket(randbyte());
      return true;
    } else {
      return false;
    }
  }

  // return id of the planet whose name matches passed string closest to currentplanet; if none, return current planet
  int find (const(char)[] s) {
    int d = int.max;
    int res = currPlanet.number;
    foreach (immutable idx, const ref p; planets) {
      if (startsWithCI(p.name, s)) {
        auto nd = currPlanet.distanceTo(p);
        if (nd < d) {
          d = nd;
          res = cast(int)idx;
        }
      }
    }
    return res;
  }

  static startsWithCI (const(char)[] s, const(char)[] pat) nothrow @trusted @nogc {
    import std.ascii : toUpper, toLower;
    if (pat.length == 0 || pat.length > s.length) return false;
    foreach (immutable idx, char ch; pat) {
      if (toLower(s.ptr[idx]) != toLower(ch)) return false;
    }
    return true;
  }

private:
  // generate system info from seed
  void makeSystem (out Planet thissys) {
    alias s = galseed;
    //Planet thissys;
    ushort pair1, pair2, pair3, pair4;
    ushort longnameflag = s.w0&64;
    const(char)* pairs1 = pairs.ptr+24; // start of pairs used by this routine

    thissys.x = s.w1>>8;
    thissys.y = s.w0>>8;

    thissys.govtype = cast(Galaxy.Gov)((s.w1>>3)&7); // bits 3, 4 & 5 of w1

    ubyte ec = (s.w0>>8)&7; // bits 8, 9 & A of w0
    if (thissys.govtype <= 1) ec |= 2;
    thissys.economy = cast(Galaxy.Economy)ec;

    thissys.techlev = cast(ubyte)(((s.w1>>8)&3)+(ec^7));
    thissys.techlev += thissys.govtype>>1;
    if (thissys.govtype&1) thissys.techlev += 1; // simulation of 6502's LSR then ADC

    thissys.population = cast(ubyte)(4*thissys.techlev+thissys.economy);
    thissys.population += thissys.govtype+1;

    thissys.productivity = cast(ushort)(((thissys.economy^7)+3)*(thissys.govtype+4));
    thissys.productivity *= thissys.population*8;

    thissys.radius = cast(ushort)(256*(((s.w2>>8)&15)+11)+thissys.x);

    thissys.goatsoupseed.a = s.w1&0xFF;
    thissys.goatsoupseed.b = s.w1>>8;
    thissys.goatsoupseed.c = s.w2&0xFF;
    thissys.goatsoupseed.d = s.w2>>8;

    pair1 = 2*((s.w2>>8)&31); s.tweak;
    pair2 = 2*((s.w2>>8)&31); s.tweak;
    pair3 = 2*((s.w2>>8)&31); s.tweak;
    pair4 = 2*((s.w2>>8)&31); s.tweak;
    // always four iterations of random number

    char[12] namebuf;
    uint nbpos;

    void putCh (char ch) nothrow @trusted @nogc {
      import std.ascii : toUpper, toLower;
      if (ch == '.') return;
      if (nbpos == 0) ch = toUpper(ch); else ch = toLower(ch);
      namebuf.ptr[nbpos++] = ch;
    }

    putCh(pairs1[pair1]);
    putCh(pairs1[pair1+1]);
    putCh(pairs1[pair2]);
    putCh(pairs1[pair2+1]);
    putCh(pairs1[pair3]);
    putCh(pairs1[pair3+1]);

    // bit 6 of ORIGINAL w0 flags a four-pair name
    if (longnameflag) {
      putCh(pairs1[pair4]);
      putCh(pairs1[pair4+1]);
    }
    thissys.name = namebuf[0..nbpos].idup;
  }

private:
  // prng for planet price fluctuations
  static uint lastrand = 0x29a;
  static void mysrand (uint seed) { lastrand = seed-1; }
  static int myrand () {
    int r;
    // As supplied by D McDonnell from SAS Insititute C
    r = (((((((((((lastrand<<3)-lastrand)<<3)+lastrand)<<1)+lastrand)<<4)-lastrand)<<1)-lastrand)+0xe60)&0x7fffffff;
    lastrand = r-1;
    return r;
  }
  static ubyte randbyte () { return (myrand()&0xFF); }
}


// ////////////////////////////////////////////////////////////////////////// //
private:

// four byte random number used for planet description
struct FastPRng {
  ubyte a, b, c, d;

pure nothrow @safe @nogc:
  int next () {
    int a, x;
    x = (this.a*2)&0xFF;
    a = x+this.c;
    if (this.a > 127) ++a;
    this.a = a&0xFF;
    this.c = cast(ubyte)x;
    a /= 256; // a = any carry left from above
    x = this.b;
    a = (a+x+this.d)&0xFF;
    this.b = cast(ubyte)a;
    this.d = cast(ubyte)x;
    return a;
  }
}


// six byte random number used as seed for planets
// initied with base seed for galaxy 1
struct GalaxySeed {
  ushort w0 = 0x5A4Au;
  ushort w1 = 0x0248u;
  ushort w2 = 0xB753u;

pure nothrow @safe @nogc:
  // create seed for the corresponding galaxy
  this (ubyte galnum) {
    galnum &= 0x07;
    foreach (immutable _; 0..galnum) nextgalaxy();
  }

  static GalaxySeed galaxy (ubyte galnum) { return GalaxySeed(galnum); }

  @property ulong seed () const {
    return
      ((cast(ulong)w0)<<2*16)|
      ((cast(ulong)w1)<<1*16)|
      (cast(ulong)w2);
  }

  @property void seed (ulong v) {
    w0 = cast(ushort)(v>>(2*16));
    w1 = cast(ushort)(v>>(1*16));
    w2 = cast(ushort)(v);
  }

  private void tweak () {
    ushort temp = cast(ushort)(cast(ushort)(w0+w1)+w2); // 2 byte aritmetic
    w0 = w1;
    w1 = w2;
    w2 = temp;
  }

  // apply to base seed; once for galaxy 2, etc.
  void nextgalaxy () {
    // rotate 8 bit number leftwards
    static ushort rotatel() (ushort x) { ushort temp = x&128; return (2*(x&127))+(temp>>7); }
    static ushort twist() (ushort x) { return cast(ushort)((256*rotatel(x>>8))+rotatel(x&255)); }
    w0 = twist(w0); // twice for galaxy 3, etc.
    w1 = twist(w1); // Eighth application gives galaxy 1 again
    w2 = twist(w2);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
struct TradeGoodIntr {
  // In 6502 version these were:
  ushort baseprice; // one byte
  short gradient; // five bits plus sign
  ushort basequant; // one byte
  ushort maskbyte; // one byte
  ushort units; // two bits
  string name; // longest="Radioactives"
}

static immutable string[8] govnames = ["Anarchy", "Feudal", "Multi-gov", "Dictatorship", "Communist", "Confederacy", "Democracy", "Corporate State"];
static immutable string[8] econnames = ["Rich Industrial", "Average Industrial", "Poor Industrial", "Mainly Industrial", "Mainly Agricultural", "Rich Agricultural", "Average Agricultural", "Poor Agricultural"];
static immutable string[3] unitnames = ["t", "kg", "g"];

/* Data for DB's price/availability generation system */
/* Base  Grad Base Mask Un   Name
   price ient quant     it */
static immutable TradeGoodIntr[17] commodities = [
  TradeGoodIntr(0x13, -0x02, 0x06, 0x01, 0, "Food"),
  TradeGoodIntr(0x14, -0x01, 0x0A, 0x03, 0, "Textiles"),
  TradeGoodIntr(0x41, -0x03, 0x02, 0x07, 0, "Radioactives"),
  TradeGoodIntr(0x28, -0x05, 0xE2, 0x1F, 0, "Slaves"),
  TradeGoodIntr(0x53, -0x05, 0xFB, 0x0F, 0, "Liquor/Wines"),
  TradeGoodIntr(0xC4, +0x08, 0x36, 0x03, 0, "Luxuries"),
  TradeGoodIntr(0xEB, +0x1D, 0x08, 0x78, 0, "Narcotics"),
  TradeGoodIntr(0x9A, +0x0E, 0x38, 0x03, 0, "Computers"),
  TradeGoodIntr(0x75, +0x06, 0x28, 0x07, 0, "Machinery"),
  TradeGoodIntr(0x4E, +0x01, 0x11, 0x1F, 0, "Alloys"),
  TradeGoodIntr(0x7C, +0x0d, 0x1D, 0x07, 0, "Firearms"),
  TradeGoodIntr(0xB0, -0x09, 0xDC, 0x3F, 0, "Furs"),
  TradeGoodIntr(0x20, -0x01, 0x35, 0x03, 0, "Minerals"),
  TradeGoodIntr(0x61, -0x01, 0x42, 0x07, 1, "Gold"),
  TradeGoodIntr(0xAB, -0x02, 0x37, 0x1F, 1, "Platinum"),
  TradeGoodIntr(0x2D, -0x01, 0xFA, 0x0F, 2, "Gem Stones"),
  TradeGoodIntr(0x35, +0x0F, 0xC0, 0x07, 0, "Alien Items"),
];


// ////////////////////////////////////////////////////////////////////////// //
// fixed (R.C): can't assume that two separate array declarations are adjacently located in memory -- unified in a sigle array
string pairs = "ABOUSEITILETSTONLONUTHNO..LEXEGEZACEBISOUSESARMAINDIREA.ERATENBERALAVETIEDORQUANTEISRION"; // dots should be nullprint characters

/* "Goat Soup" planetary description string code - adapted from Christian Pinder's reverse engineered sources. */
static immutable string[5][36] descList = [
/* 81 */  ["fabled", "notable", "well known", "famous", "noted"],
/* 82 */  ["very", "mildly", "most", "reasonably", ""],
/* 83 */  ["ancient", "\x95", "great", "vast", "pink"],
/* 84 */  ["\x9E \x9D plantations", "mountains", "\x9C", "\x94 forests", "oceans"],
/* 85 */  ["shyness", "silliness", "mating traditions", "loathing of \x86", "love for \x86"],
/* 86 */  ["food blenders", "tourists", "poetry", "discos", "\x8E"],
/* 87 */  ["talking tree", "crab", "bat", "lobst", "\xB2"],
/* 88 */  ["beset", "plagued", "ravaged", "cursed", "scourged"],
/* 89 */  ["\x96 civil war", "\x9B \x98 \x99s", "a \x9B disease", "\x96 earthquakes", "\x96 solar activity"],
/* 8A */  ["its \x83 \x84", "the \xB1 \x98 \x99", "its inhabitants' \x9A \x85", "\xA1", "its \x8D \x8E"],
/* 8B */  ["juice", "brandy", "water", "brew", "gargle blasters"],
/* 8C */  ["\xB2", "\xB1 \x99", "\xB1 \xB2", "\xB1 \x9B", "\x9B \xB2"],
/* 8D */  ["fabulous", "exotic", "hoopy", "unusual", "exciting"],
/* 8E */  ["cuisine", "night life", "casinos", "sit coms", " \xA1 "],
/* 8F */  ["\xB0", "The planet \xB0", "The world \xB0", "This planet", "This world"],
/* 90 */  ["n unremarkable", " boring", " dull", " tedious", " revolting"],
/* 91 */  ["planet", "world", "place", "little planet", "dump"],
/* 92 */  ["wasp", "moth", "grub", "ant", "\xB2"],
/* 93 */  ["poet", "arts graduate", "yak", "snail", "slug"],
/* 94 */  ["tropical", "dense", "rain", "impenetrable", "exuberant"],
/* 95 */  ["funny", "wierd", "unusual", "strange", "peculiar"],
/* 96 */  ["frequent", "occasional", "unpredictable", "dreadful", "deadly"],
/* 97 */  ["\x82 \x81 for \x8A", "\x82 \x81 for \x8A and \x8A", "\x88 by \x89", "\x82 \x81 for \x8A but \x88 by \x89", "a\x90 \x91"],
/* 98 */  ["\x9B", "mountain", "edible", "tree", "spotted"],
/* 99 */  ["\x9F", "\xA0", "\x87oid", "\x93", "\x92"],
/* 9A */  ["ancient", "exceptional", "eccentric", "ingrained", "\x95"],
/* 9B */  ["killer", "deadly", "evil", "lethal", "vicious"],
/* 9C */  ["parking meters", "dust clouds", "ice bergs", "rock formations", "volcanoes"],
/* 9D */  ["plant", "tulip", "banana", "corn", "\xB2weed"],
/* 9E */  ["\xB2", "\xB1 \xB2", "\xB1 \x9B", "inhabitant", "\xB1 \xB2"],
/* 9F */  ["shrew", "beast", "bison", "snake", "wolf"],
/* A0 */  ["leopard", "cat", "monkey", "goat", "fish"],
/* A1 */  ["\x8C \x8B", "\xB1 \x9F \xA2", "its \x8D \xA0 \xA2", "\xA3 \xA4", "\x8C \x8B"],
/* A2 */  ["meat", "cutlet", "steak", "burgers", "soup"],
/* A3 */  ["ice", "mud", "Zero-G", "vacuum", "\xB1 ultra"],
/* A4 */  ["hockey", "cricket", "karate", "polo", "tennis"]
];

/* B0 = <planet name>
   B1 = <planet name>ian
   B2 = <random name>
*/

string goatSoup (ref FastPRng prng, string source, in ref Galaxy.Planet psy) {
  import std.ascii : toLower;
  string res;

  void addCh (char ch) {
    if (!ch) return; // just in case
    if (ch <= ' ') ch = ' ';
    if (ch == ' ') {
      if (res.length == 0 || res[$-1] <= ' ') return;
      res ~= ch;
    } else if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z') || (ch >= '0' && ch <= '9')) {
      res ~= ch;
    } else {
      // punctuation -- remove trailing spaces
      while (res.length && res[$-1] <= ' ') res = res[0..$-1];
      res ~= ch;
    }
  }

  while (source.length) {
    char c = source[0];
    source = source[1..$];
    if (c < 0x80) {
      addCh(c);
    } else {
      if (c <= 0xA4) {
        int rnd = prng.next;
        res ~= goatSoup(prng, descList[c-0x81][(rnd >= 0x33)+(rnd >= 0x66)+(rnd >= 0x99)+(rnd >= 0xCC)], psy);
      } else
        switch (c) {
          case 0xB0: // planet name
            addCh(psy.name[0]);
            foreach (char ch; psy.name[1..$]) addCh(toLower(ch));
            break;
          case 0xB1: // <planet name>ian
            addCh(psy.name[0]);
            string t = psy.name[1..$];
            while (t.length) {
              if (t.length > 1 || (t[0] != 'e' && t[0] != 'i')) addCh(toLower(t[0]));
              t = t[1..$];
            }
            res ~= "ian";
            break;
          case 0xB2: // random name
            int len = prng.next&3;
            for (int i = 0; i <= len; ++i) {
              int x = prng.next&0x3e;
              /* fixed (R.C.): transform chars to lowercase unless
                 first char of first pair, or second char of first
                 pair when first char was not printed */
              char p1 = pairs[x];
              char p2 = pairs[x+1];
              if (p1 != '.') {
                if (i) p1 = toLower(p1);
                addCh(p1);
              }
              if (p2 != '.') {
                if (i || p1 != '.') p2 = toLower(p2);
                addCh(p2);
              }
            }
            break;
          default: assert(0);
      }
    }
  }
  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
version(elite_world_test) unittest {
  import std.stdio;
  auto uni = Galaxy(0);

  conwriteln("System: ", uni.currPlanet.name);
  conwriteln("Position: (", uni.currPlanet.x, ",", uni.currPlanet.y, ")");
  conwriteln("Economy: ", uni.currPlanet.economyName);
  conwriteln("Government: ", uni.currPlanet.govName);
  conwriteln("Tech Level: ", uni.currPlanet.techlev+1);
  conwriteln("Turnover: ", uni.currPlanet.productivity);
  conwriteln("Radius: ", uni.currPlanet.radius);
  conwriteln("Population: ", uni.currPlanet.population/10, ".", uni.currPlanet.population%10, " Billion");
  conwriteln(uni.currPlanet.description);

  conwriteln("--------------------------------");
  foreach (immutable idx; 0..Galaxy.Goods.max+1) {
    auto good = Galaxy.good(idx);
    conwritefln!"%-12s  %3s.%s  %2s%s"(good.name, uni.currPlanet.price[idx]/10, uni.currPlanet.price[idx]%10, uni.currPlanet.quantity[idx], good.unitName);
  }
}
