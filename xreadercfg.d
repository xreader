/* Written by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module xreadercfg;

import arsd.simpledisplay;
import arsd.image;

import iv.nanovega;
import iv.strex;
import iv.vfs;
import iv.vfs.io;

import iv.sq3;

import xiniz;
import xmodel;
import booktext;

private import std.path : buildPath;


// ////////////////////////////////////////////////////////////////////////// //
__gshared string RcDir = "~/.xreader";
//__gshared string stateFileName;
__gshared int currentFileId = -1;
__gshared Database filedb;

private enum SQLPragmas =
//"PRAGMA page_size=512;\n"~
//"PRAGMA threads=3;\n"~
"PRAGMA case_sensitive_like=OFF;\n"~
"PRAGMA foreign_keys=OFF;\n"~
"PRAGMA secure_delete=OFF;\n"~
"PRAGMA trusted_schema=OFF;\n"~
"PRAGMA writable_schema=OFF;\n"~
"PRAGMA auto_vacuum=NONE;\n"~
"PRAGMA encoding='UTF-8';\n"~
"PRAGMA synchronous=OFF;\n"~
"PRAGMA journal_mode=WAL;\n";

private enum SQLSchema =
"CREATE TABLE IF NOT EXISTS known_files (\n"~
"    id INTEGER PRIMARY KEY /* file id */\n"~
"  , lastread INTEGER NOT NULL DEFAULT 0 /* unixtime; 0 means 'hidden' */\n"~
"  , wordindex INTEGER NOT NULL DEFAULT 0 /* last file position */\n"~
"  , filename TEXT NOT NULL UNIQUE /* filename without a path */\n"~
"  , fullpath TEXT /* disk file name, purely informational, may be absent */\n"~
"  , title TEXT /* book title */\n"~
"  , author TEXT /* book author */\n"~
"  , seqname TEXT /* sequence name */\n"~
"  , seqnum INTEGER NOT NULL DEFAULT 0 /* sequence index */\n"~
");\n"~
"\n"~
"CREATE INDEX IF NOT EXISTS files_by_lastread ON known_files (lastread);\n"~
"\n"~
"\n"~
"CREATE TABLE IF NOT EXISTS flubusta_cache (\n"~
"    id INTEGER PRIMARY KEY /* record id */\n"~
"  , flibusta_id TEXT NOT NULL UNIQUE\n"~
"  , filename TEXT NOT NULL\n"~
");\n"~
"";

shared static this () {
  import std.path;
  RcDir = RcDir.expandTilde.absolutePath;
  try { import std.file; mkdirRecurse(RcDir); } catch (Exception) {}
  filedb.openEx(buildPath(RcDir, "xreader.db"), Database.Mode.ReadWriteCreate,
                SQLPragmas, SQLSchema);
  //writeln(RcDir);
}

shared static ~this () {
  filedb.close();
}

__gshared float shipAngle = 0;
__gshared bool showShip = false;

__gshared EliteModel shipModel;


// ////////////////////////////////////////////////////////////////////////// //
__gshared string textFontName = "~/ttf/ms/arial.ttf:noaa";
__gshared string textiFontName = "~/ttf/ms/ariali.ttf:noaa";
__gshared string textbFontName = "~/ttf/ms/arialbd.ttf:noaa";
__gshared string textzFontName = "~/ttf/ms/arialbi.ttf:noaa";
__gshared string monoFontName = "~/ttf/ms/cour.ttf:noaa";
__gshared string monoiFontName = "~/ttf/ms/couri.ttf:noaa";
__gshared string monobFontName = "~/ttf/ms/courbd.ttf:noaa";
__gshared string monozFontName = "~/ttf/ms/courbi.ttf:noaa";
__gshared string uiFontName = "~/ttf/ms/verdana.ttf:noaa";
__gshared string galmapFontName = "~/ttf/ms/verdana.ttf:noaa";
__gshared int GWidth = 900;
__gshared int GHeight = 1024;
__gshared int fsizeText = 24;
__gshared int fsizeUI = 16;
__gshared int fGalMapSize = 16;
__gshared bool sbLeft = false;
__gshared bool interAllowed = false;

__gshared bool flagNanoAA = false;
__gshared bool flagNanoSS = false;
__gshared bool flagNanoFAA = false;

__gshared NVGColor colorDim = NVGColor(0, 0, 0, 0);
__gshared NVGColor colorBack = NVGColor(0x2a, 0x2a, 0x2a);
__gshared NVGColor colorText = NVGColor(0xff, 0x7f, 0x00);
__gshared NVGColor colorTextHi = NVGColor(0xff, 0xff, 0x00);
__gshared NVGColor colorTextHref = NVGColor(0, 0, 0x80);

__gshared int uiFont, galmapFont;
__gshared int textFont, textiFont, textbFont, textzFont;
__gshared int monoFont, monoiFont, monobFont, monozFont;

__gshared bool optJustify = false;



enum MinWinWidth = 320;
enum MinWinHeight = 240;


// ////////////////////////////////////////////////////////////////////////// //
int updateFileLastReadTime (string fname, string author, string title,
                              string sequence, uint seqnum)
{
  if (fname.length == 0) return -1;
  import std.datetime;
  import std.path : baseName, expandTilde, absolutePath;
  string filename = fname.baseName;
  string fullname = fname.expandTilde.absolutePath;
  long epoch = cast(long)Clock.currTime.toUnixTime();
  if (author.length == 0) author = "";
  if (title.length == 0) title = "";
  auto stmt = filedb.statement(`
    INSERT INTO known_files
            ( lastread, filename, fullpath, author, title, seqname, seqnum)
      VALUES(:lastread,:filename,:fullpath,:author,:title,:seqname,:seqnum)
    ON CONFLICT(filename)
      DO UPDATE
      SET   lastread=:lastread, fullpath=:fullpath
          , author=:author, title=:title
          , seqname=:seqname, seqnum=:seqnum
    RETURNING id
  `);
  stmt
    .bind(":lastread", epoch)
    .bindText(":filename", filename)
    .bindText(":fullpath", fullname)
    .bindText(":author", author)
    .bindText(":title", title)
    .bindText(":seqname", sequence)
    .bind(":seqnum", seqnum);
  int id = -1;
  foreach (auto row; stmt.range) {
    id = row.id!int;
  }
  return id;
}


string getLatestFileName () {
  auto stmt = filedb.statement(`
    SELECT MAX(lastread) AS lrd, fullpath AS fullpath FROM known_files
    WHERE lastread>0 AND fullpath<>''
  `);
  string res;
  foreach (auto row; stmt.range) {
    res = row.fullpath!string;
  }
  return res;
}


int getCurrentFileWordIndex () {
  if (currentFileId <= 0) return -1;
  auto stmt = filedb.statement(`
    SELECT wordindex AS wordindex FROM known_files
    WHERE id=:id
    LIMIT 1
  `);
  int widx = -1;
  foreach (auto row; stmt.bind(":id", currentFileId).range) {
    widx = row.wordindex!int;
  }
  return widx;
}


void updateCurrentFileWordIndex (int widx) {
  if (currentFileId <= 0 || widx < 0) return;
  auto stmt = filedb.statement(`
    UPDATE known_files SET wordindex=:widx
    WHERE id=:id
  `);
  stmt
    .bind(":id", currentFileId)
    .bind(":widx", widx)
    .doAll();
}


// ////////////////////////////////////////////////////////////////////////// //
BookInfo[] loadDetailedHistory () {
  BookInfo[] res;
  int[] hideFileIds;
  scope(exit) delete hideFileIds;
  bool hasUpdates = false;
  filedb.beginTransaction();
  scope(exit) filedb.commitTransaction();
  try {
    import std.path, std.file : exists;
    auto stmt = filedb.statement(`
      SELECT id AS id, fullpath AS fullpath, author AS author, title AS title
             , seqname AS seqname, seqnum AS seqnum
      FROM known_files
      WHERE lastread>0 AND fullpath<>''
      ORDER BY lastread
    `);
    foreach (auto row; stmt.range) {
      import std.file : exists, isFile;
      string diskfile = row.fullpath!string;
      bool ok = false;
      try {
        version(none) {
          import std.stdio;
          writeln(diskfile, "; exists=", diskfile.exists, "; isFile=", diskfile.isFile,
                  "; author=", row.author!string,
                  "; title=", row.title!string,
                  "; seqname=", row.seqname!string,
                  "; seqnum=", row.seqnum!uint,
                  "; id=", row.id!int);
        }
        if (diskfile.exists && diskfile.isFile) {
          BookInfo bi;
          bi.author = row.author!string;
          bi.title = row.title!string;
          bi.seqname = row.seqname!string;
          bi.seqnum = row.seqnum!uint;
          bi.diskfile = diskfile;
          bi.id = row.id!int;
          if (bi.author.length == 0 && bi.title.length == 0) {
            // update it
            bi = loadBookInfo(diskfile);
            bi.diskfile = diskfile;
            bi.id = row.id!int;
            bi.needupdate = true;
            hasUpdates = true;
          }
          res ~= bi;
          ok = true;
        }
      } catch (Exception) {}
      if (!ok) hideFileIds ~= row.id!int;
    }
    // hide missing files
    if (hideFileIds.length) {
      stmt = filedb.statement(`
        UPDATE known_files
        SET lastread=0
        WHERE id=:fid
      `);
      foreach (int fid; hideFileIds) {
        stmt.bind(":fid", fid).doAll();
      }
    }
    if (hasUpdates) {
      stmt = filedb.statement(`
        UPDATE known_files
        SET author=:author, title=:title, seqname=:seqname, seqnum=:seqnum
        WHERE id=:fid
      `);
      foreach (const ref BookInfo bi; res) {
        if (!bi.needupdate) continue;
        stmt
          .bind(":fid", bi.id)
          .bindText(":author", bi.author)
          .bindText(":title", bi.title)
          .bindText(":seqname", bi.seqname)
          .bind(":seqnum", bi.seqnum)
          .doAll();
      }
    }
  } catch (Exception) {}
  return res;
}


void removeFileFromHistory (const(char)[] filename) {
  auto stmt = filedb.statement(`
    UPDATE known_files
    SET lastread=0
    WHERE fullpath=:filename
  `);
  stmt
    .bindText(":filename", filename)
    .doAll();
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared string[] eliteShipFiles;


void loadEliteShips () {
  import std.path;
  import std.path : extension;
  vfsAddPak(buildPath(RcDir, "eliteships.zip"), "oxm:");
  //scope(exit) vfsRemovePak(buildPath(RcDir, "eliteships.zip"), "oxm:");
  foreach (immutable idx, ref de; vfsFileList) {
    //writeln("<", de.name, ">");
    if (de.name.length > 4 && de.name[0..4] == "oxm:" && strEquCI(de.name.extension, ".oxm")) {
      /*
      import core.memory : GC;
      try {
        auto mdl = new EliteModel(de.name);
        mdl.freeData();
        mdl.destroy;
        //eliteShips ~= mdl;
        eliteShipFiles ~= de.name;
      } catch (Exception e) {}
      GC.collect();
      */
      eliteShipFiles ~= de.name;
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
VFile fwritef(A...) (VFile fl, string fmt, /*lazy*/ A args) {
  import std.string : format;
  auto s = format(fmt, args);
  if (s.length) fl.rawWriteExact(s[]);
  return fl;
}


// ////////////////////////////////////////////////////////////////////////// //
void readConfig () {
  import std.path;
  VFile fl;
  try {
    fl = VFile(buildPath(RcDir, "config.ui"));
  } catch (Exception) {
    try {
      try { import std.file; mkdirRecurse(RcDir); } catch (Exception) {}
      fl = VFile(buildPath(RcDir, "config.ui"), "w");
      fl.fwritef("width=%s\n", GWidth);
      fl.fwritef("height=%s\n", GHeight);
      fl.fwritef("font-text=%s\n", textFontName);
      fl.fwritef("font-text-italic=%s\n", textiFontName);
      fl.fwritef("font-text-bold=%s\n", textbFontName);
      fl.fwritef("font-text-bold-italic=%s\n", textzFontName);
      fl.fwritef("font-mono=%s\n", monoFontName);
      fl.fwritef("font-mono-italic=%s\n", monoiFontName);
      fl.fwritef("font-mono-bold=%s\n", monobFontName);
      fl.fwritef("font-mono-bold-italic=%s\n", monozFontName);
      fl.fwritef("font-ui=%s\n", uiFontName);
      fl.fwritef("font-galmap=%s\n", galmapFontName);
      fl.fwritef("text-size=%s\n", fsizeText);
      fl.fwritef("ui-text-size=%s\n", fsizeUI);
      fl.fwritef("galmap-text-size=%s\n", fGalMapSize);
      fl.fwritef("scrollbar-on-left=%s\n", sbLeft);
      fl.fwritef("interference=%s\n", interAllowed);
      fl.fwritef("color-dim=0x%08x\n", colorDim.asUintARGB);
      fl.fwritef("color-back=0x%08x\n", colorBack.asUintARGB);
      fl.fwritef("color-text=0x%08x\n", colorText.asUintARGB);
      fl.fwritef("color-text-hi=0x%08x\n", colorTextHi.asUintARGB);
      fl.fwritef("color-text-href=0x%08x\n", colorTextHref.asUintARGB);
      fl.fwritef("nano-aa=%s\n", flagNanoAA);
      fl.fwritef("nano-ss=%s\n", flagNanoSS);
      fl.fwritef("nano-faa=%s\n", flagNanoFAA);
      fl.fwritef("justify-text=%s\n", optJustify);
    } catch (Exception) {}
    return;
  }
  xiniParse(fl,
    "font-text", &textFontName,
    "font-text-italic", &textiFontName,
    "font-text-bold", &textbFontName,
    "font-text-bold-italic", &textzFontName,
    "font-text-italic-bold", &textzFontName,
    "font-mono", &monoFontName,
    "font-mono-italic", &monoiFontName,
    "font-mono-bold", &monobFontName,
    "font-mono-bold-italic", &monozFontName,
    "font-mono-italic-bold", &monozFontName,
    "font-ui", &uiFontName,
    "font-galmap", &galmapFontName,
    "width", &GWidth,
    "height", &GHeight,
    "text-size", &fsizeText,
    "ui-text-size", &fsizeUI,
    "galmap-text-size", &fGalMapSize,
    "scrollbar-on-left", &sbLeft,
    "interference", &interAllowed,
    "color-dim", &colorDim,
    "color-back", &colorBack,
    "color-text", &colorText,
    "color-text-hi", &colorTextHi,
    "color-text-href", &colorTextHref,
    "nano-aa", &flagNanoAA,
    "nano-ss", &flagNanoSS,
    "nano-faa", &flagNanoFAA,
    "justify-text", &optJustify,
  );
}


// ////////////////////////////////////////////////////////////////////////// //
bool isComment (const(char)[] s) {
  while (s.length && s.ptr[0] <= ' ') s = s[1..$];
  return (s.length > 0 && s.ptr[0] == '#');
}
